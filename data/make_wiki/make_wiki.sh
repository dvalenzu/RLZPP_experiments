#!/usr/bin/env bash                                                                                 
set -o errexit                                                                                      
set -o nounset                                                                                      
set -o pipefail                                                                                     
                                                                                                    
while read FILE 
do
{
  if [[ ! -f "${FILE}" ]]; then
    wget  https://dumps.wikimedia.org/enwiki/20180801/${FILE}.7z
    #./tmp/7za e ${FILE}.7z
    7za e ${FILE}.7z
  else
    echo "File in place"
  fi
} done < "./files.txt" 

rm -f Wiki

while read FILE 
do
{
  cat ${FILE} >> Wiki
} done < "./files.txt"
rm -rf enwiki-20180801-pages-meta-history*

mw Wiki ../large/
