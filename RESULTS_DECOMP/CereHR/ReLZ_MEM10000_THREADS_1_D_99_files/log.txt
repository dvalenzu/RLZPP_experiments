
--- computing reference lenght based on memory limit:
--- With 10000MB of  memory, the reference length will be: 1048575999 symbols
RLZ parser intitialized with:
Ignore     : 0
Ref        : 1048575999
Partitions : 3
Memory(MB) : 10000
RLZ parsing...
Building SA...
We will read blocks of size: 1835008001
Reading block 0/13
Block read in 7.42093 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/13
Block read in 12.7344 (s)
Waiting for all tasks to be done...
Done.
Reading block 2/13
Block read in 12.7197 (s)

*
Starting parallel parsing in block...Parallel parse in 43.6888 (s)
Writing phrases... 
Phrases written  in 0.032367 (s)
Waiting for all tasks to be done...
Done.
Reading block 3/13
Block read in 12.769 (s)

*
Starting parallel parsing in block...Parallel parse in 92.1525 (s)
Writing phrases... 
Phrases written  in 0.083351 (s)
Waiting for all tasks to be done...
Done.
Reading block 4/13
Block read in 12.8019 (s)

*
Starting parallel parsing in block...Parallel parse in 132.308 (s)
Writing phrases... 
Phrases written  in 0.131655 (s)
Waiting for all tasks to be done...
Done.
Reading block 5/13
Block read in 12.9694 (s)

*
Starting parallel parsing in block...Parallel parse in 168.26 (s)
Writing phrases... 
Phrases written  in 0.174136 (s)
Waiting for all tasks to be done...
Done.
Reading block 6/13
Block read in 12.8353 (s)

*
Starting parallel parsing in block...Parallel parse in 202.355 (s)
Writing phrases... 
Phrases written  in 0.21845 (s)
Waiting for all tasks to be done...
Done.
Reading block 7/13
Block read in 12.9286 (s)

*
Starting parallel parsing in block...Parallel parse in 234.732 (s)
Writing phrases... 
Phrases written  in 0.272988 (s)
Waiting for all tasks to be done...
Done.
Reading block 8/13
Block read in 12.8814 (s)

*
Starting parallel parsing in block...Parallel parse in 266.723 (s)
Writing phrases... 
Phrases written  in 0.318644 (s)
Waiting for all tasks to be done...
Done.
Reading block 9/13
Block read in 12.8174 (s)

*
Starting parallel parsing in block...Parallel parse in 296.618 (s)
Writing phrases... 
Phrases written  in 0.363479 (s)
Waiting for all tasks to be done...
Done.
Reading block 10/13
Block read in 13.0164 (s)

*
Starting parallel parsing in block...Parallel parse in 326.465 (s)
Writing phrases... 
Phrases written  in 0.40779 (s)
Waiting for all tasks to be done...
Done.
Reading block 11/13
Block read in 12.9071 (s)

*
Starting parallel parsing in block...Parallel parse in 355.556 (s)
Writing phrases... 
Phrases written  in 0.452236 (s)
Waiting for all tasks to be done...
Done.
Reading block 12/13
Block read in 12.9272 (s)

*
Starting parallel parsing in block...Parallel parse in 386.701 (s)
Writing phrases... 
Phrases written  in 0.495823 (s)
Waiting for all tasks to be done...
Done.
Reading block 13/13
Block read in 3.25463 (s)

*
Starting parallel parsing in block...Parallel parse in 416.731 (s)
Writing phrases... 
Phrases written  in 0.534942 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 107.597 (s)
Writing phrases... 
Phrases written  in 0.130421 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 10000MB of  memory, the reference length will be: 393215999 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 1817998
Ref        : 101290913
Partitions : 3
Memory(MB) : 10000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 103108911
Reading block 0/0
Block read in 0.238262 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 17
IM Random Access to sums file will use at most (MB): 208
 Random Access to Sum Files IM (delta compressed)./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.TMP.rlztmp.sums
***
***
***
POS MBE::
MBE would use (bits):297282111
***
LEN MBE::
MBE would use (bits):80729856
***
***
***
You are using 28.5437 bits per integer. 
You are using 12.5341 bits per integer. 
ABOUT TO CALL:
yes | /home/local/dvalenzu/Repos/Code/ReLZ_experiments/ext/ReLZ//ext/FiniteStateEntropy/programs/fse ./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.len ./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.len.fse > ./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.len.log 2>&1
*****
yes: standard output: Broken pipe
ABOUT TO CALL:
yes | /home/local/dvalenzu/Repos/Code/ReLZ_experiments/ext/ReLZ//ext/FiniteStateEntropy/programs/fse ./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.pos.1 ./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.pos.1.fse > ./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.pos.1.log 2>&1
*****
yes: standard output: Broken pipe
ABOUT TO CALL:
yes | /home/local/dvalenzu/Repos/Code/ReLZ_experiments/ext/ReLZ//ext/FiniteStateEntropy/programs/fse ./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.pos.2 ./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.pos.2.fse > ./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.pos.2.log 2>&1
*****
yes: standard output: Broken pipe
Input succesfully compressed, output written in: ./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 1817998
Factors RLZ          : 101290913
Factors Total        : 103108911
--------------------------------------------
Time SA          : 118.084
Time LZ77 of ref : 28.5278
Time RLZ parse   : 3191.14
-------------------------------
Time RLZ total   : 3337.75
--------------------------------------
Rlz TOTAL time 	: 3337.98
Pack      time 	: 3.64144
Recursion time 	: 117.681
ReParse   time 	: 6.01961
Encode    time 	: 0.862613
--------------------------------------
TOTAL     time 	: 3466.18

1-th level report: 

Factors skipped      : 1817998
Factors 77'ed in ref : 8379099
Factors RLZ          : 0
Factors Total        : 10197097
--------------------------------------------
Time SA          : 112.956
Time LZ77 of ref : 4.62916
Time RLZ parse   : 0
-------------------------------
Time RLZ total   : 117.585


Final Wtime(s)    : 3466.18
N Phrases         : 10197097
Compression ratio : 0.00247638
Ideal       ratio : 0.00200851
Bits per char     : 0.0198111
Ideal bpc         : 0.0160681
Bits output       : 466067232
seconds per MiB   : 0.154494
seconds per GiB   : 158.201
MiB per second    : 6.47276
GiB per second    : 0.00632105
Remaining arguments are: 
./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file ./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.decompressed
Input: ./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file
Output: ./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.decompressed
ABOUT TO CALL:
yes | /home/local/dvalenzu/Repos/Code/ReLZ_experiments/ext/ReLZ//ext/FiniteStateEntropy/programs/fse -d ./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.len.fse ./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.len > ./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.len.fse.log 2>&1
*****
yes: standard output: Broken pipe
ABOUT TO CALL:
yes | /home/local/dvalenzu/Repos/Code/ReLZ_experiments/ext/ReLZ//ext/FiniteStateEntropy/programs/fse -d ./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.pos.1.fse ./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.pos.1 > ./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.pos.1.fse.log 2>&1
*****
yes: standard output: Broken pipe
ABOUT TO CALL:
yes | /home/local/dvalenzu/Repos/Code/ReLZ_experiments/ext/ReLZ//ext/FiniteStateEntropy/programs/fse -d ./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.pos.2.fse ./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.pos.2 > ./RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.pos.2.fse.log 2>&1
*****
yes: standard output: Broken pipe
Split to vbyte will process: 10197097phrases


Input filename = /home/local/dvalenzu/Repos/Code/ReLZ_experiments/TMP_VBYTE_FILE_kkk
Output filename = /home/local/dvalenzu/Repos/Code/ReLZ_experiments/RESULTS_DECOMP/CereHR/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.decompressed
Input size = 63832520 (60.9MiB)
Max disk use = 1152921504606846976 (1099511627776.0MiB)
RAM use = 3758096384 (3584.0MiB)
RAM use (excl. buffer) = 3724541952 (3552.0MiB)
Max segment size = 3724541952 (3552.0MiB)

Process part 1:
  Total parsing data decoded so far: 0.00%
  Total text decoded so far: 0 (0.00MiB)
  Permute phrases by source:   Permute phrases by source: time = 0.04s, I/O = 229.34MiB/s  Permute phrases by source: time = 0.08s, I/O = 271.42MiB/s  Permute phrases by source: time = 0.12s, I/O = 297.13MiB/s  Permute phrases by source: time = 0.17s, I/O = 313.33MiB/s  Permute phrases by source: time = 0.21s, I/O = 323.00MiB/s  Permute phrases by source: time = 0.25s, I/O = 329.15MiB/s  Permute phrases by source: time = 0.29s, I/O = 332.92MiB/s  Permute phrases by source: time = 0.33s, I/O = 335.91MiB/s  Permute phrases by source: time = 0.37s, I/O = 337.75MiB/s  Permute phrases by source: 100.0%, time = 0.40s, I/O = 338.64MiB/s
  Processing this part will decode 22435.8MiB of text (100.00% of parsing)
  Last part = TRUE
  Decode segment 1/7 [0..3724541952):
    Selfdecode segment:     Selfdecode segment: time = 0.05s, I/O = 118.47MiB/s    Selfdecode segment: time = 0.75s, I/O = 16.42MiB/s    Selfdecode segment: time = 1.28s, I/O = 12.75MiB/s
    Distribute chunks:     Distribute chunks: time = 0.25s, I/O = 1915.86MiB/s    Distribute chunks: time = 0.51s, I/O = 1743.55MiB/s    Distribute chunks: time = 0.74s, I/O = 1610.59MiB/s    Distribute chunks: time = 0.94s, I/O = 1508.38MiB/s    Distribute chunks: time = 1.13s, I/O = 1427.35MiB/s    Distribute chunks: time = 1.22s, I/O = 1389.92MiB/s
    Write segment to disk: 2.26s, I/O = 1575.04MiB/s
    Decode the nearby phrases for next segment: 0.15s
  Decode segment 2/7 [3724541952..7449083904):
    Load segment chunks:     Load segment chunks: 2.3%, time = 0.00s, I/O = 3134.80MiB/s    Load segment chunks: 4.3%, time = 0.01s, I/O = 3005.46MiB/s    Load segment chunks: 5.1%, time = 0.01s, I/O = 2434.46MiB/s    Load segment chunks: 5.9%, time = 0.01s, I/O = 2122.09MiB/s    Load segment chunks: 6.6%, time = 0.02s, I/O = 1935.78MiB/s    Load segment chunks: 7.4%, time = 0.02s, I/O = 1813.76MiB/s    Load segment chunks: 8.2%, time = 0.02s, I/O = 1724.14MiB/s    Load segment chunks: 9.0%, time = 0.03s, I/O = 1658.61MiB/s    Load segment chunks: 9.8%, time = 0.03s, I/O = 1609.06MiB/s    Load segment chunks: 10.5%, time = 0.03s, I/O = 1567.35MiB/s    Load segment chunks: 11.3%, time = 0.04s, I/O = 1532.49MiB/s    Load segment chunks: 12.1%, time = 0.04s, I/O = 1502.78MiB/s    Load segment chunks: 12.9%, time = 0.04s, I/O = 1478.43MiB/s    Load segment chunks: 13.7%, time = 0.05s, I/O = 1458.82MiB/s    Load segment chunks: 14.4%, time = 0.05s, I/O = 1440.33MiB/s    Load segment chunks: 15.6%, time = 0.05s, I/O = 1461.75MiB/s    Load segment chunks: 16.4%, time = 0.06s, I/O = 1445.81MiB/s    Load segment chunks: 17.2%, time = 0.06s, I/O = 1432.31MiB/s    Load segment chunks: 17.9%, time = 0.06s, I/O = 1419.69MiB/s    Load segment chunks: 18.7%, time = 0.07s, I/O = 1409.07MiB/s    Load segment chunks: 19.5%, time = 0.07s, I/O = 1398.78MiB/s    Load segment chunks: 20.3%, time = 0.07s, I/O = 1389.91MiB/s    Load segment chunks: 21.1%, time = 0.08s, I/O = 1381.29MiB/s    Load segment chunks: 21.8%, time = 0.08s, I/O = 1373.56MiB/s    Load segment chunks: 22.6%, time = 0.08s, I/O = 1367.18MiB/s    Load segment chunks: 23.4%, time = 0.09s, I/O = 1361.27MiB/s    Load segment chunks: 24.2%, time = 0.09s, I/O = 1354.24MiB/s    Load segment chunks: 25.0%, time = 0.09s, I/O = 1349.57MiB/s    Load segment chunks: 25.7%, time = 0.10s, I/O = 1344.36MiB/s    Load segment chunks: 26.5%, time = 0.10s, I/O = 1340.18MiB/s    Load segment chunks: 27.3%, time = 0.10s, I/O = 1335.44MiB/s    Load segment chunks: 28.1%, time = 0.11s, I/O = 1331.39MiB/s    Load segment chunks: 28.9%, time = 0.11s, I/O = 1327.18MiB/s    Load segment chunks: 29.6%, time = 0.11s, I/O = 1323.00MiB/s    Load segment chunks: 30.4%, time = 0.12s, I/O = 1319.41MiB/s    Load segment chunks: 31.2%, time = 0.12s, I/O = 1315.85MiB/s    Load segment chunks: 32.0%, time = 0.12s, I/O = 1312.35MiB/s    Load segment chunks: 32.8%, time = 0.13s, I/O = 1309.04MiB/s    Load segment chunks: 33.6%, time = 0.13s, I/O = 1305.91MiB/s    Load segment chunks: 34.3%, time = 0.14s, I/O = 1302.89MiB/s    Load segment chunks: 35.1%, time = 0.14s, I/O = 1300.20MiB/s    Load segment chunks: 35.9%, time = 0.14s, I/O = 1297.90MiB/s    Load segment chunks: 36.7%, time = 0.15s, I/O = 1295.47MiB/s    Load segment chunks: 37.5%, time = 0.15s, I/O = 1293.12MiB/s    Load segment chunks: 38.2%, time = 0.15s, I/O = 1291.18MiB/s    Load segment chunks: 39.0%, time = 0.16s, I/O = 1288.71MiB/s    Load segment chunks: 39.8%, time = 0.16s, I/O = 1287.00MiB/s    Load segment chunks: 40.6%, time = 0.16s, I/O = 1285.21MiB/s    Load segment chunks: 41.4%, time = 0.17s, I/O = 1283.50MiB/s    Load segment chunks: 42.1%, time = 0.17s, I/O = 1281.72MiB/s    Load segment chunks: 42.9%, time = 0.17s, I/O = 1279.94MiB/s    Load segment chunks: 43.7%, time = 0.18s, I/O = 1278.20MiB/s    Load segment chunks: 44.5%, time = 0.18s, I/O = 1276.67MiB/s    Load segment chunks: 45.3%, time = 0.18s, I/O = 1275.35MiB/s    Load segment chunks: 46.0%, time = 0.19s, I/O = 1273.61MiB/s    Load segment chunks: 46.8%, time = 0.19s, I/O = 1272.27MiB/s    Load segment chunks: 47.6%, time = 0.19s, I/O = 1270.67MiB/s    Load segment chunks: 48.4%, time = 0.20s, I/O = 1269.22MiB/s    Load segment chunks: 49.2%, time = 0.20s, I/O = 1267.82MiB/s    Load segment chunks: 49.9%, time = 0.20s, I/O = 1266.57MiB/s    Load segment chunks: 50.7%, time = 0.21s, I/O = 1265.24MiB/s    Load segment chunks: 51.5%, time = 0.21s, I/O = 1263.88MiB/s    Load segment chunks: 52.3%, time = 0.21s, I/O = 1262.68MiB/s    Load segment chunks: 53.1%, time = 0.22s, I/O = 1261.46MiB/s    Load segment chunks: 54.2%, time = 0.22s, I/O = 1269.39MiB/s    Load segment chunks: 55.0%, time = 0.22s, I/O = 1267.88MiB/s    Load segment chunks: 55.8%, time = 0.23s, I/O = 1266.63MiB/s    Load segment chunks: 56.6%, time = 0.23s, I/O = 1265.50MiB/s    Load segment chunks: 57.3%, time = 0.23s, I/O = 1264.70MiB/s    Load segment chunks: 58.1%, time = 0.24s, I/O = 1263.84MiB/s    Load segment chunks: 58.9%, time = 0.24s, I/O = 1262.74MiB/s    Load segment chunks: 59.7%, time = 0.24s, I/O = 1261.84MiB/s    Load segment chunks: 60.5%, time = 0.25s, I/O = 1260.98MiB/s    Load segment chunks: 61.2%, time = 0.25s, I/O = 1260.05MiB/s    Load segment chunks: 62.0%, time = 0.25s, I/O = 1259.16MiB/s    Load segment chunks: 62.8%, time = 0.26s, I/O = 1257.98MiB/s    Load segment chunks: 63.6%, time = 0.26s, I/O = 1256.92MiB/s    Load segment chunks: 64.4%, time = 0.26s, I/O = 1256.06MiB/s    Load segment chunks: 65.2%, time = 0.27s, I/O = 1255.37MiB/s    Load segment chunks: 65.9%, time = 0.27s, I/O = 1254.17MiB/s    Load segment chunks: 66.7%, time = 0.27s, I/O = 1253.12MiB/s    Load segment chunks: 67.5%, time = 0.28s, I/O = 1252.15MiB/s    Load segment chunks: 68.3%, time = 0.28s, I/O = 1251.42MiB/s    Load segment chunks: 69.1%, time = 0.28s, I/O = 1250.69MiB/s    Load segment chunks: 69.8%, time = 0.29s, I/O = 1249.86MiB/s    Load segment chunks: 70.6%, time = 0.29s, I/O = 1249.10MiB/s    Load segment chunks: 71.4%, time = 0.29s, I/O = 1248.34MiB/s    Load segment chunks: 72.2%, time = 0.30s, I/O = 1247.73MiB/s    Load segment chunks: 73.0%, time = 0.30s, I/O = 1247.17MiB/s    Load segment chunks: 73.7%, time = 0.30s, I/O = 1246.73MiB/s    Load segment chunks: 74.5%, time = 0.31s, I/O = 1246.06MiB/s    Load segment chunks: 75.3%, time = 0.31s, I/O = 1245.37MiB/s    Load segment chunks: 76.1%, time = 0.31s, I/O = 1244.67MiB/s    Load segment chunks: 76.9%, time = 0.32s, I/O = 1244.17MiB/s    Load segment chunks: 77.6%, time = 0.32s, I/O = 1243.40MiB/s    Load segment chunks: 78.4%, time = 0.32s, I/O = 1242.64MiB/s    Load segment chunks: 79.2%, time = 0.33s, I/O = 1241.93MiB/s    Load segment chunks: 80.0%, time = 0.33s, I/O = 1241.05MiB/s    Load segment chunks: 80.8%, time = 0.33s, I/O = 1240.49MiB/s    Load segment chunks: 81.5%, time = 0.34s, I/O = 1239.80MiB/s    Load segment chunks: 82.3%, time = 0.34s, I/O = 1239.15MiB/s    Load segment chunks: 83.1%, time = 0.34s, I/O = 1238.48MiB/s    Load segment chunks: 83.9%, time = 0.35s, I/O = 1237.86MiB/s    Load segment chunks: 84.7%, time = 0.35s, I/O = 1237.42MiB/s    Load segment chunks: 85.4%, time = 0.35s, I/O = 1236.90MiB/s    Load segment chunks: 86.2%, time = 0.36s, I/O = 1236.30MiB/s    Load segment chunks: 87.0%, time = 0.36s, I/O = 1235.78MiB/s    Load segment chunks: 87.8%, time = 0.36s, I/O = 1235.45MiB/s    Load segment chunks: 88.9%, time = 0.37s, I/O = 1240.53MiB/s    Load segment chunks: 89.7%, time = 0.37s, I/O = 1239.91MiB/s    Load segment chunks: 90.5%, time = 0.37s, I/O = 1239.43MiB/s    Load segment chunks: 91.3%, time = 0.38s, I/O = 1238.87MiB/s    Load segment chunks: 92.1%, time = 0.38s, I/O = 1238.41MiB/s    Load segment chunks: 92.8%, time = 0.38s, I/O = 1237.78MiB/s    Load segment chunks: 93.6%, time = 0.39s, I/O = 1237.26MiB/s    Load segment chunks: 94.4%, time = 0.39s, I/O = 1236.62MiB/s    Load segment chunks: 95.2%, time = 0.39s, I/O = 1236.05MiB/s    Load segment chunks: 96.0%, time = 0.40s, I/O = 1235.46MiB/s    Load segment chunks: 96.8%, time = 0.40s, I/O = 1235.01MiB/s    Load segment chunks: 97.5%, time = 0.41s, I/O = 1234.56MiB/s    Load segment chunks: 98.3%, time = 0.41s, I/O = 1234.21MiB/s    Load segment chunks: 99.1%, time = 0.41s, I/O = 1233.77MiB/s    Load segment chunks: 99.9%, time = 0.42s, I/O = 1233.38MiB/s    Load segment chunks: 100.0%, time = 0.42s, I/O = 1225.18MiB/s    Load segment chunks: 100.0%, time = 0.42s, I/O = 1215.52MiB/s    Load segment chunks: 100.0%, time = 0.43s, I/O = 1205.95MiB/s    Load segment chunks: 100.0%, time = 0.43s, I/O = 1198.89MiB/s
    Selfdecode segment:     Selfdecode segment: time = 0.57s, I/O = 4.05MiB/s
    Distribute chunks:     Distribute chunks: time = 0.00s, I/O = 654.57MiB/s
    Write segment to disk: 4.04s, I/O = 878.72MiB/s
    Decode the nearby phrases for next segment: 0.09s
  Decode segment 3/7 [7449083904..11173625856):
    Load segment chunks:     Load segment chunks: 3.0%, time = 0.00s, I/O = 2952.03MiB/s    Load segment chunks: 5.5%, time = 0.01s, I/O = 2923.98MiB/s    Load segment chunks: 6.5%, time = 0.01s, I/O = 2373.56MiB/s    Load segment chunks: 7.5%, time = 0.01s, I/O = 2077.71MiB/s    Load segment chunks: 8.5%, time = 0.02s, I/O = 1901.57MiB/s    Load segment chunks: 9.5%, time = 0.02s, I/O = 1779.28MiB/s    Load segment chunks: 10.5%, time = 0.02s, I/O = 1693.62MiB/s    Load segment chunks: 11.5%, time = 0.03s, I/O = 1630.51MiB/s    Load segment chunks: 12.5%, time = 0.03s, I/O = 1582.98MiB/s    Load segment chunks: 13.5%, time = 0.03s, I/O = 1544.98MiB/s    Load segment chunks: 14.5%, time = 0.04s, I/O = 1511.64MiB/s    Load segment chunks: 15.5%, time = 0.04s, I/O = 1485.31MiB/s    Load segment chunks: 17.0%, time = 0.05s, I/O = 1505.12MiB/s    Load segment chunks: 18.0%, time = 0.05s, I/O = 1481.97MiB/s    Load segment chunks: 19.0%, time = 0.05s, I/O = 1461.96MiB/s    Load segment chunks: 20.0%, time = 0.06s, I/O = 1443.68MiB/s    Load segment chunks: 20.9%, time = 0.06s, I/O = 1427.96MiB/s    Load segment chunks: 21.9%, time = 0.06s, I/O = 1413.52MiB/s    Load segment chunks: 22.9%, time = 0.07s, I/O = 1400.56MiB/s    Load segment chunks: 23.9%, time = 0.07s, I/O = 1389.49MiB/s    Load segment chunks: 24.9%, time = 0.07s, I/O = 1379.12MiB/s    Load segment chunks: 25.9%, time = 0.08s, I/O = 1369.21MiB/s    Load segment chunks: 26.9%, time = 0.08s, I/O = 1360.68MiB/s    Load segment chunks: 27.9%, time = 0.08s, I/O = 1352.97MiB/s    Load segment chunks: 28.9%, time = 0.09s, I/O = 1345.83MiB/s    Load segment chunks: 29.9%, time = 0.09s, I/O = 1339.85MiB/s    Load segment chunks: 30.9%, time = 0.09s, I/O = 1333.65MiB/s    Load segment chunks: 31.9%, time = 0.10s, I/O = 1327.68MiB/s    Load segment chunks: 32.9%, time = 0.10s, I/O = 1322.34MiB/s    Load segment chunks: 33.9%, time = 0.10s, I/O = 1317.46MiB/s    Load segment chunks: 34.9%, time = 0.11s, I/O = 1312.56MiB/s    Load segment chunks: 35.9%, time = 0.11s, I/O = 1308.25MiB/s    Load segment chunks: 36.9%, time = 0.11s, I/O = 1303.52MiB/s    Load segment chunks: 37.9%, time = 0.12s, I/O = 1299.75MiB/s    Load segment chunks: 38.9%, time = 0.12s, I/O = 1295.58MiB/s    Load segment chunks: 39.9%, time = 0.12s, I/O = 1291.99MiB/s    Load segment chunks: 40.9%, time = 0.13s, I/O = 1289.03MiB/s    Load segment chunks: 41.9%, time = 0.13s, I/O = 1286.46MiB/s    Load segment chunks: 42.9%, time = 0.13s, I/O = 1283.65MiB/s    Load segment chunks: 43.9%, time = 0.14s, I/O = 1280.83MiB/s    Load segment chunks: 44.9%, time = 0.14s, I/O = 1278.03MiB/s    Load segment chunks: 45.9%, time = 0.14s, I/O = 1275.48MiB/s    Load segment chunks: 47.4%, time = 0.15s, I/O = 1286.23MiB/s    Load segment chunks: 48.4%, time = 0.15s, I/O = 1283.37MiB/s    Load segment chunks: 49.4%, time = 0.15s, I/O = 1280.57MiB/s    Load segment chunks: 50.4%, time = 0.16s, I/O = 1278.07MiB/s    Load segment chunks: 51.4%, time = 0.16s, I/O = 1275.63MiB/s    Load segment chunks: 52.4%, time = 0.16s, I/O = 1273.61MiB/s    Load segment chunks: 53.4%, time = 0.17s, I/O = 1271.66MiB/s    Load segment chunks: 54.4%, time = 0.17s, I/O = 1269.40MiB/s    Load segment chunks: 55.4%, time = 0.18s, I/O = 1267.54MiB/s    Load segment chunks: 56.4%, time = 0.18s, I/O = 1265.96MiB/s    Load segment chunks: 57.4%, time = 0.18s, I/O = 1264.10MiB/s    Load segment chunks: 58.4%, time = 0.19s, I/O = 1262.48MiB/s    Load segment chunks: 59.4%, time = 0.19s, I/O = 1260.59MiB/s    Load segment chunks: 60.3%, time = 0.19s, I/O = 1258.71MiB/s    Load segment chunks: 61.3%, time = 0.20s, I/O = 1256.90MiB/s    Load segment chunks: 62.3%, time = 0.20s, I/O = 1255.23MiB/s    Load segment chunks: 63.3%, time = 0.20s, I/O = 1253.91MiB/s    Load segment chunks: 64.3%, time = 0.21s, I/O = 1252.40MiB/s    Load segment chunks: 65.3%, time = 0.21s, I/O = 1251.43MiB/s    Load segment chunks: 66.3%, time = 0.21s, I/O = 1250.24MiB/s    Load segment chunks: 67.3%, time = 0.22s, I/O = 1248.98MiB/s    Load segment chunks: 68.3%, time = 0.22s, I/O = 1247.77MiB/s    Load segment chunks: 69.3%, time = 0.22s, I/O = 1246.28MiB/s    Load segment chunks: 70.3%, time = 0.23s, I/O = 1244.86MiB/s    Load segment chunks: 71.3%, time = 0.23s, I/O = 1243.59MiB/s    Load segment chunks: 72.3%, time = 0.23s, I/O = 1242.18MiB/s    Load segment chunks: 73.3%, time = 0.24s, I/O = 1240.86MiB/s    Load segment chunks: 74.3%, time = 0.24s, I/O = 1239.40MiB/s    Load segment chunks: 75.3%, time = 0.24s, I/O = 1238.04MiB/s    Load segment chunks: 76.8%, time = 0.25s, I/O = 1245.05MiB/s    Load segment chunks: 77.8%, time = 0.25s, I/O = 1244.16MiB/s    Load segment chunks: 78.8%, time = 0.25s, I/O = 1243.14MiB/s    Load segment chunks: 79.8%, time = 0.26s, I/O = 1242.08MiB/s    Load segment chunks: 80.8%, time = 0.26s, I/O = 1240.95MiB/s    Load segment chunks: 81.8%, time = 0.26s, I/O = 1239.93MiB/s    Load segment chunks: 82.8%, time = 0.27s, I/O = 1238.79MiB/s    Load segment chunks: 83.8%, time = 0.27s, I/O = 1237.33MiB/s    Load segment chunks: 84.8%, time = 0.27s, I/O = 1236.40MiB/s    Load segment chunks: 85.8%, time = 0.28s, I/O = 1235.12MiB/s    Load segment chunks: 86.8%, time = 0.28s, I/O = 1234.06MiB/s    Load segment chunks: 87.8%, time = 0.29s, I/O = 1233.07MiB/s    Load segment chunks: 88.8%, time = 0.29s, I/O = 1232.32MiB/s    Load segment chunks: 89.8%, time = 0.29s, I/O = 1231.50MiB/s    Load segment chunks: 90.8%, time = 0.30s, I/O = 1230.63MiB/s    Load segment chunks: 91.8%, time = 0.30s, I/O = 1229.79MiB/s    Load segment chunks: 92.8%, time = 0.30s, I/O = 1228.89MiB/s    Load segment chunks: 93.8%, time = 0.31s, I/O = 1227.94MiB/s    Load segment chunks: 94.8%, time = 0.31s, I/O = 1226.92MiB/s    Load segment chunks: 95.8%, time = 0.31s, I/O = 1226.10MiB/s    Load segment chunks: 96.8%, time = 0.32s, I/O = 1225.13MiB/s    Load segment chunks: 97.8%, time = 0.32s, I/O = 1224.38MiB/s    Load segment chunks: 98.8%, time = 0.32s, I/O = 1223.60MiB/s    Load segment chunks: 100.0%, time = 0.33s, I/O = 1226.13MiB/s    Load segment chunks: 100.0%, time = 0.33s, I/O = 1213.67MiB/s    Load segment chunks: 100.0%, time = 0.33s, I/O = 1201.30MiB/s    Load segment chunks: 100.0%, time = 0.34s, I/O = 1189.03MiB/s    Load segment chunks: 100.0%, time = 0.34s, I/O = 1182.22MiB/s
    Selfdecode segment:     Selfdecode segment: time = 0.58s, I/O = 4.34MiB/s
    Distribute chunks:     Distribute chunks: time = 0.00s, I/O = 808.45MiB/s
    Write segment to disk: 4.54s, I/O = 782.14MiB/s
    Decode the nearby phrases for next segment: 0.10s
  Decode segment 4/7 [11173625856..14898167808):
    Load segment chunks:     Load segment chunks: 2.1%, time = 0.02s, I/O = 241.95MiB/s    Load segment chunks: 3.5%, time = 0.04s, I/O = 236.33MiB/s    Load segment chunks: 4.9%, time = 0.06s, I/O = 242.33MiB/s    Load segment chunks: 6.3%, time = 0.07s, I/O = 257.91MiB/s    Load segment chunks: 7.7%, time = 0.08s, I/O = 264.34MiB/s    Load segment chunks: 9.1%, time = 0.10s, I/O = 259.54MiB/s    Load segment chunks: 10.5%, time = 0.11s, I/O = 261.74MiB/s    Load segment chunks: 12.0%, time = 0.13s, I/O = 258.11MiB/s    Load segment chunks: 13.4%, time = 0.15s, I/O = 260.10MiB/s    Load segment chunks: 14.8%, time = 0.16s, I/O = 265.89MiB/s    Load segment chunks: 16.2%, time = 0.17s, I/O = 275.04MiB/s    Load segment chunks: 17.6%, time = 0.18s, I/O = 276.29MiB/s    Load segment chunks: 19.0%, time = 0.20s, I/O = 271.96MiB/s    Load segment chunks: 20.4%, time = 0.21s, I/O = 274.03MiB/s    Load segment chunks: 21.8%, time = 0.22s, I/O = 278.42MiB/s    Load segment chunks: 23.2%, time = 0.24s, I/O = 275.99MiB/s    Load segment chunks: 24.6%, time = 0.25s, I/O = 280.45MiB/s    Load segment chunks: 26.0%, time = 0.26s, I/O = 281.27MiB/s    Load segment chunks: 27.4%, time = 0.27s, I/O = 285.29MiB/s    Load segment chunks: 28.8%, time = 0.28s, I/O = 289.75MiB/s    Load segment chunks: 30.2%, time = 0.30s, I/O = 289.11MiB/s    Load segment chunks: 31.6%, time = 0.31s, I/O = 287.60MiB/s    Load segment chunks: 33.1%, time = 0.33s, I/O = 289.05MiB/s    Load segment chunks: 35.2%, time = 0.34s, I/O = 292.59MiB/s    Load segment chunks: 36.6%, time = 0.35s, I/O = 294.20MiB/s    Load segment chunks: 38.0%, time = 0.37s, I/O = 290.35MiB/s    Load segment chunks: 39.4%, time = 0.38s, I/O = 291.68MiB/s    Load segment chunks: 40.8%, time = 0.40s, I/O = 289.99MiB/s    Load segment chunks: 42.2%, time = 0.41s, I/O = 291.65MiB/s    Load segment chunks: 43.6%, time = 0.43s, I/O = 291.54MiB/s    Load segment chunks: 45.0%, time = 0.44s, I/O = 293.09MiB/s    Load segment chunks: 46.4%, time = 0.45s, I/O = 292.17MiB/s    Load segment chunks: 47.8%, time = 0.47s, I/O = 291.97MiB/s    Load segment chunks: 49.2%, time = 0.48s, I/O = 289.49MiB/s    Load segment chunks: 50.6%, time = 0.50s, I/O = 288.10MiB/s    Load segment chunks: 52.0%, time = 0.52s, I/O = 286.53MiB/s    Load segment chunks: 53.4%, time = 0.53s, I/O = 286.04MiB/s    Load segment chunks: 54.9%, time = 0.55s, I/O = 282.26MiB/s    Load segment chunks: 56.3%, time = 0.56s, I/O = 283.57MiB/s    Load segment chunks: 57.7%, time = 0.58s, I/O = 283.49MiB/s    Load segment chunks: 59.1%, time = 0.59s, I/O = 283.57MiB/s    Load segment chunks: 60.5%, time = 0.60s, I/O = 284.93MiB/s    Load segment chunks: 61.9%, time = 0.62s, I/O = 285.66MiB/s    Load segment chunks: 63.3%, time = 0.63s, I/O = 284.53MiB/s    Load segment chunks: 65.4%, time = 0.65s, I/O = 285.03MiB/s    Load segment chunks: 66.8%, time = 0.67s, I/O = 283.99MiB/s    Load segment chunks: 68.2%, time = 0.68s, I/O = 283.80MiB/s    Load segment chunks: 69.6%, time = 0.70s, I/O = 282.59MiB/s    Load segment chunks: 71.0%, time = 0.71s, I/O = 283.35MiB/s    Load segment chunks: 72.4%, time = 0.72s, I/O = 284.34MiB/s    Load segment chunks: 73.8%, time = 0.74s, I/O = 283.79MiB/s    Load segment chunks: 75.2%, time = 0.75s, I/O = 284.84MiB/s    Load segment chunks: 76.7%, time = 0.76s, I/O = 286.04MiB/s    Load segment chunks: 78.1%, time = 0.78s, I/O = 286.26MiB/s    Load segment chunks: 79.5%, time = 0.79s, I/O = 286.85MiB/s    Load segment chunks: 80.9%, time = 0.80s, I/O = 288.48MiB/s    Load segment chunks: 82.3%, time = 0.81s, I/O = 289.34MiB/s    Load segment chunks: 83.7%, time = 0.82s, I/O = 290.34MiB/s    Load segment chunks: 85.1%, time = 0.83s, I/O = 290.47MiB/s    Load segment chunks: 86.5%, time = 0.85s, I/O = 290.14MiB/s    Load segment chunks: 87.9%, time = 0.86s, I/O = 290.30MiB/s    Load segment chunks: 89.3%, time = 0.87s, I/O = 290.30MiB/s    Load segment chunks: 91.4%, time = 0.89s, I/O = 292.25MiB/s    Load segment chunks: 92.8%, time = 0.90s, I/O = 293.61MiB/s    Load segment chunks: 94.2%, time = 0.91s, I/O = 293.72MiB/s    Load segment chunks: 95.6%, time = 0.92s, I/O = 294.55MiB/s    Load segment chunks: 97.0%, time = 0.94s, I/O = 294.27MiB/s    Load segment chunks: 98.5%, time = 0.95s, I/O = 294.97MiB/s    Load segment chunks: 100.0%, time = 0.96s, I/O = 296.21MiB/s    Load segment chunks: 100.0%, time = 0.96s, I/O = 295.36MiB/s
    Selfdecode segment:     Selfdecode segment: time = 0.60s, I/O = 4.34MiB/s
    Distribute chunks:     Distribute chunks: time = 0.00s, I/O = 895.29MiB/s
    Write segment to disk: 4.34s, I/O = 818.83MiB/s
    Decode the nearby phrases for next segment: 0.09s
  Decode segment 5/7 [14898167808..18622709760):
    Load segment chunks:     Load segment chunks: 2.7%, time = 0.11s, I/O = 55.12MiB/s    Load segment chunks: 4.5%, time = 0.15s, I/O = 65.36MiB/s    Load segment chunks: 6.2%, time = 0.19s, I/O = 72.32MiB/s    Load segment chunks: 8.0%, time = 0.23s, I/O = 79.13MiB/s    Load segment chunks: 9.8%, time = 0.26s, I/O = 84.60MiB/s    Load segment chunks: 11.6%, time = 0.28s, I/O = 92.40MiB/s    Load segment chunks: 13.4%, time = 0.30s, I/O = 99.82MiB/s    Load segment chunks: 15.2%, time = 0.32s, I/O = 106.74MiB/s    Load segment chunks: 16.9%, time = 0.34s, I/O = 112.71MiB/s    Load segment chunks: 18.7%, time = 0.35s, I/O = 118.59MiB/s    Load segment chunks: 20.5%, time = 0.37s, I/O = 124.07MiB/s    Load segment chunks: 22.3%, time = 0.39s, I/O = 129.65MiB/s    Load segment chunks: 24.1%, time = 0.40s, I/O = 135.60MiB/s    Load segment chunks: 25.8%, time = 0.41s, I/O = 139.96MiB/s    Load segment chunks: 27.6%, time = 0.43s, I/O = 145.77MiB/s    Load segment chunks: 29.4%, time = 0.44s, I/O = 150.37MiB/s    Load segment chunks: 31.2%, time = 0.45s, I/O = 154.15MiB/s    Load segment chunks: 33.0%, time = 0.47s, I/O = 158.05MiB/s    Load segment chunks: 35.7%, time = 0.49s, I/O = 164.48MiB/s    Load segment chunks: 37.4%, time = 0.50s, I/O = 168.21MiB/s    Load segment chunks: 39.2%, time = 0.52s, I/O = 170.58MiB/s    Load segment chunks: 41.0%, time = 0.53s, I/O = 172.94MiB/s    Load segment chunks: 42.8%, time = 0.54s, I/O = 176.93MiB/s    Load segment chunks: 44.6%, time = 0.56s, I/O = 179.19MiB/s    Load segment chunks: 46.3%, time = 0.57s, I/O = 181.77MiB/s    Load segment chunks: 48.1%, time = 0.59s, I/O = 183.84MiB/s    Load segment chunks: 49.9%, time = 0.60s, I/O = 186.30MiB/s    Load segment chunks: 51.7%, time = 0.62s, I/O = 187.16MiB/s    Load segment chunks: 53.5%, time = 0.63s, I/O = 190.71MiB/s    Load segment chunks: 55.3%, time = 0.64s, I/O = 193.40MiB/s    Load segment chunks: 57.0%, time = 0.65s, I/O = 195.74MiB/s    Load segment chunks: 58.8%, time = 0.67s, I/O = 198.48MiB/s    Load segment chunks: 60.6%, time = 0.68s, I/O = 200.23MiB/s    Load segment chunks: 62.4%, time = 0.69s, I/O = 202.58MiB/s    Load segment chunks: 65.1%, time = 0.71s, I/O = 205.92MiB/s    Load segment chunks: 66.8%, time = 0.72s, I/O = 207.91MiB/s    Load segment chunks: 68.6%, time = 0.74s, I/O = 208.40MiB/s    Load segment chunks: 70.4%, time = 0.75s, I/O = 210.79MiB/s    Load segment chunks: 72.2%, time = 0.76s, I/O = 212.07MiB/s    Load segment chunks: 74.0%, time = 0.77s, I/O = 214.70MiB/s    Load segment chunks: 75.8%, time = 0.79s, I/O = 215.92MiB/s    Load segment chunks: 77.5%, time = 0.80s, I/O = 217.14MiB/s    Load segment chunks: 79.3%, time = 0.82s, I/O = 217.21MiB/s    Load segment chunks: 81.1%, time = 0.84s, I/O = 216.49MiB/s    Load segment chunks: 82.9%, time = 0.86s, I/O = 216.34MiB/s    Load segment chunks: 84.7%, time = 0.87s, I/O = 217.45MiB/s    Load segment chunks: 86.5%, time = 0.89s, I/O = 217.98MiB/s    Load segment chunks: 88.2%, time = 0.91s, I/O = 218.22MiB/s    Load segment chunks: 90.0%, time = 0.92s, I/O = 218.57MiB/s    Load segment chunks: 92.7%, time = 0.94s, I/O = 220.35MiB/s    Load segment chunks: 94.5%, time = 0.96s, I/O = 221.12MiB/s    Load segment chunks: 96.3%, time = 0.98s, I/O = 221.34MiB/s    Load segment chunks: 98.0%, time = 0.99s, I/O = 222.27MiB/s    Load segment chunks: 100.0%, time = 1.00s, I/O = 224.05MiB/s    Load segment chunks: 100.0%, time = 1.01s, I/O = 223.15MiB/s
    Selfdecode segment:     Selfdecode segment: time = 0.60s, I/O = 4.40MiB/s
    Distribute chunks:     Distribute chunks: time = 0.00s, I/O = 946.40MiB/s
    Write segment to disk: 4.19s, I/O = 846.88MiB/s
    Decode the nearby phrases for next segment: 0.09s
  Decode segment 6/7 [18622709760..22347251712):
    Load segment chunks:     Load segment chunks: 3.2%, time = 0.04s, I/O = 150.88MiB/s    Load segment chunks: 5.4%, time = 0.06s, I/O = 181.80MiB/s    Load segment chunks: 7.5%, time = 0.07s, I/O = 200.34MiB/s    Load segment chunks: 9.7%, time = 0.09s, I/O = 210.02MiB/s    Load segment chunks: 11.8%, time = 0.10s, I/O = 219.67MiB/s    Load segment chunks: 14.0%, time = 0.11s, I/O = 227.47MiB/s    Load segment chunks: 16.1%, time = 0.12s, I/O = 242.07MiB/s    Load segment chunks: 18.3%, time = 0.13s, I/O = 252.23MiB/s    Load segment chunks: 20.5%, time = 0.15s, I/O = 249.80MiB/s    Load segment chunks: 22.6%, time = 0.17s, I/O = 249.78MiB/s    Load segment chunks: 24.8%, time = 0.19s, I/O = 248.57MiB/s    Load segment chunks: 26.9%, time = 0.20s, I/O = 248.26MiB/s    Load segment chunks: 29.1%, time = 0.22s, I/O = 248.13MiB/s    Load segment chunks: 33.4%, time = 0.23s, I/O = 264.03MiB/s    Load segment chunks: 36.6%, time = 0.25s, I/O = 275.88MiB/s    Load segment chunks: 38.7%, time = 0.25s, I/O = 283.53MiB/s    Load segment chunks: 39.8%, time = 0.26s, I/O = 284.94MiB/s    Load segment chunks: 40.9%, time = 0.26s, I/O = 287.22MiB/s    Load segment chunks: 43.1%, time = 0.28s, I/O = 289.21MiB/s    Load segment chunks: 45.2%, time = 0.29s, I/O = 289.97MiB/s    Load segment chunks: 47.4%, time = 0.31s, I/O = 286.14MiB/s    Load segment chunks: 49.5%, time = 0.33s, I/O = 283.00MiB/s    Load segment chunks: 51.7%, time = 0.34s, I/O = 281.70MiB/s    Load segment chunks: 53.8%, time = 0.35s, I/O = 284.59MiB/s    Load segment chunks: 56.0%, time = 0.36s, I/O = 288.02MiB/s    Load segment chunks: 58.1%, time = 0.38s, I/O = 286.75MiB/s    Load segment chunks: 60.3%, time = 0.39s, I/O = 285.51MiB/s    Load segment chunks: 62.4%, time = 0.40s, I/O = 288.19MiB/s    Load segment chunks: 65.7%, time = 0.42s, I/O = 287.55MiB/s    Load segment chunks: 67.8%, time = 0.44s, I/O = 287.41MiB/s    Load segment chunks: 70.0%, time = 0.45s, I/O = 287.41MiB/s    Load segment chunks: 72.1%, time = 0.47s, I/O = 287.47MiB/s    Load segment chunks: 74.3%, time = 0.48s, I/O = 289.09MiB/s    Load segment chunks: 76.4%, time = 0.49s, I/O = 288.34MiB/s    Load segment chunks: 78.6%, time = 0.51s, I/O = 288.73MiB/s    Load segment chunks: 80.7%, time = 0.52s, I/O = 289.63MiB/s    Load segment chunks: 82.9%, time = 0.54s, I/O = 286.38MiB/s    Load segment chunks: 85.0%, time = 0.55s, I/O = 287.32MiB/s    Load segment chunks: 87.2%, time = 0.57s, I/O = 284.76MiB/s    Load segment chunks: 89.3%, time = 0.58s, I/O = 286.91MiB/s    Load segment chunks: 92.6%, time = 0.60s, I/O = 286.29MiB/s    Load segment chunks: 94.7%, time = 0.61s, I/O = 290.13MiB/s    Load segment chunks: 96.9%, time = 0.62s, I/O = 288.71MiB/s    Load segment chunks: 100.0%, time = 0.64s, I/O = 291.17MiB/s    Load segment chunks: 100.0%, time = 0.64s, I/O = 289.09MiB/s
    Selfdecode segment:     Selfdecode segment: time = 0.60s, I/O = 4.40MiB/s
    Distribute chunks:     Distribute chunks: time = 0.00s, I/O = 586.08MiB/s
    Write segment to disk: 4.53s, I/O = 784.61MiB/s
    Decode the nearby phrases for next segment: 0.09s
  Decode segment 7/7 [22347251712..23525618844):
    Load segment chunks:     Load segment chunks: 11.3%, time = 0.07s, I/O = 89.14MiB/s    Load segment chunks: 18.9%, time = 0.10s, I/O = 104.62MiB/s    Load segment chunks: 26.4%, time = 0.13s, I/O = 105.44MiB/s    Load segment chunks: 34.0%, time = 0.15s, I/O = 119.31MiB/s    Load segment chunks: 41.5%, time = 0.17s, I/O = 126.16MiB/s    Load segment chunks: 49.0%, time = 0.20s, I/O = 131.96MiB/s    Load segment chunks: 56.6%, time = 0.21s, I/O = 140.15MiB/s    Load segment chunks: 64.1%, time = 0.23s, I/O = 145.37MiB/s    Load segment chunks: 71.7%, time = 0.26s, I/O = 148.29MiB/s    Load segment chunks: 79.2%, time = 0.27s, I/O = 155.04MiB/s    Load segment chunks: 86.8%, time = 0.29s, I/O = 160.17MiB/s    Load segment chunks: 94.3%, time = 0.31s, I/O = 161.23MiB/s    Load segment chunks: 100.0%, time = 0.32s, I/O = 165.87MiB/s
    Selfdecode segment:     Selfdecode segment: time = 0.13s, I/O = 4.14MiB/s
    Write segment to disk: 1.30s, I/O = 866.64MiB/s
    Decode the nearby phrases for next segment: 0.00s


Computation finished. Summary:
  I/O volume = 27228870348 (1.16B/B)
  number of decoded phrases: 10197097
  average phrase length = 2307.09
  length of decoded text: 23525618844 (22435.78MiB)
  elapsed time: 35.87s (0.0016s/MiB)
  speed: 625.44MiB/s
Decompression t(s)    : 36.374327898
