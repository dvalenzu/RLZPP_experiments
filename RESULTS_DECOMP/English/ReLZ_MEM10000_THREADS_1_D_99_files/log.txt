
--- computing reference lenght based on memory limit:
--- With 10000MB of  memory, the reference length will be: 1048575999 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 0
Ref        : 209715200
Partitions : 3
Memory(MB) : 10000
RLZ parsing...
Building SA...
We will read blocks of size: 209715200
Reading block 0/0
Block read in 0.097594 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
***
***
***
POS MBE::
MBE would use (bits):355447032
***
LEN MBE::
MBE would use (bits):56154858
***
***
***
You are using 26.6658 bits per integer. 
You are using 5.05298 bits per integer. 
ABOUT TO CALL:
yes | /home/local/dvalenzu/Repos/Code/ReLZ_experiments/ext/ReLZ//ext/FiniteStateEntropy/programs/fse ./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.TMP.rlz.len ./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.TMP.rlz.len.fse > ./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.TMP.rlz.len.log 2>&1
*****
yes: standard output: Broken pipe
ABOUT TO CALL:
yes | /home/local/dvalenzu/Repos/Code/ReLZ_experiments/ext/ReLZ//ext/FiniteStateEntropy/programs/fse ./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.TMP.rlz.pos.1 ./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.TMP.rlz.pos.1.fse > ./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.TMP.rlz.pos.1.log 2>&1
*****
yes: standard output: Broken pipe
ABOUT TO CALL:
yes | /home/local/dvalenzu/Repos/Code/ReLZ_experiments/ext/ReLZ//ext/FiniteStateEntropy/programs/fse ./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.TMP.rlz.pos.2 ./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.TMP.rlz.pos.2.fse > ./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.TMP.rlz.pos.2.log 2>&1
*****
yes: standard output: Broken pipe
Suffix: .pos.1.fse
Suffix: .pos.2.fse
Suffix: .len.fse
Suffix: .z
Input succesfully compressed, output written in: ./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file
Parsing performed in 1 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 13971133
Factors RLZ          : 0
Factors Total        : 13971133
--------------------------------------------
Time SA          : 20.8342
Time LZ77 of ref : 7.25365
Time RLZ parse   : 9.99891e-07
-------------------------------
Time RLZ total   : 28.0879


Final Wtime(s)    : 29.0622
N Phrases         : 13971133
Compression ratio : 0.263103
Ideal       ratio : 0.245334
Bits per char     : 2.10483
Ideal bpc         : 1.96267
Bits output       : 441413816
seconds per MiB   : 0.145311
seconds per GiB   : 148.798
MiB per second    : 6.8818
GiB per second    : 0.00672051
Remaining arguments are: 
./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file ./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.decompressed
Input: ./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file
Output: ./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.decompressed
ABOUT TO CALL:
yes | /home/local/dvalenzu/Repos/Code/ReLZ_experiments/ext/ReLZ//ext/FiniteStateEntropy/programs/fse -d ./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.len.fse ./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.len > ./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.len.fse.log 2>&1
*****
yes: standard output: Broken pipe
ABOUT TO CALL:
yes | /home/local/dvalenzu/Repos/Code/ReLZ_experiments/ext/ReLZ//ext/FiniteStateEntropy/programs/fse -d ./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.pos.1.fse ./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.pos.1 > ./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.pos.1.fse.log 2>&1
*****
yes: standard output: Broken pipe
ABOUT TO CALL:
yes | /home/local/dvalenzu/Repos/Code/ReLZ_experiments/ext/ReLZ//ext/FiniteStateEntropy/programs/fse -d ./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.pos.2.fse ./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.pos.2 > ./RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.pos.2.fse.log 2>&1
*****
yes: standard output: Broken pipe
Split to vbyte will process: 13971133phrases


Input filename = /home/local/dvalenzu/Repos/Code/ReLZ_experiments/TMP_VBYTE_FILE_kkk
Output filename = /home/local/dvalenzu/Repos/Code/ReLZ_experiments/RESULTS_DECOMP/English/ReLZ_MEM10000_THREADS_1_D_99_files/compressed_file.decompressed
Input size = 68878568 (65.7MiB)
Max disk use = 1152921504606846976 (1099511627776.0MiB)
RAM use = 3758096384 (3584.0MiB)
RAM use (excl. buffer) = 3724541952 (3552.0MiB)
Max segment size = 3724541952 (3552.0MiB)

Process part 1:
  Total parsing data decoded so far: 0.00%
  Total text decoded so far: 0 (0.00MiB)
  Permute phrases by source:   Permute phrases by source: time = 0.04s, I/O = 243.90MiB/s  Permute phrases by source: time = 0.07s, I/O = 279.27MiB/s  Permute phrases by source: time = 0.11s, I/O = 295.48MiB/s  Permute phrases by source: time = 0.14s, I/O = 304.27MiB/s  Permute phrases by source: time = 0.17s, I/O = 310.25MiB/s  Permute phrases by source: time = 0.20s, I/O = 314.56MiB/s  Permute phrases by source: time = 0.24s, I/O = 317.66MiB/s  Permute phrases by source: time = 0.27s, I/O = 320.23MiB/s  Permute phrases by source: time = 0.30s, I/O = 322.17MiB/s  Permute phrases by source: time = 0.33s, I/O = 323.76MiB/s  Permute phrases by source: time = 0.37s, I/O = 325.27MiB/s  Permute phrases by source: time = 0.40s, I/O = 326.52MiB/s  Permute phrases by source: time = 0.43s, I/O = 327.57MiB/s  Permute phrases by source: 100.0%, time = 0.44s, I/O = 327.86MiB/s
  Processing this part will decode 200.0MiB of text (100.00% of parsing)
  Last part = TRUE
  Decode segment 1/1 [0..209715200):
    Selfdecode segment:     Selfdecode segment: time = 0.05s, I/O = 121.37MiB/s    Selfdecode segment: time = 0.12s, I/O = 95.85MiB/s    Selfdecode segment: time = 0.20s, I/O = 86.43MiB/s    Selfdecode segment: time = 0.29s, I/O = 81.16MiB/s    Selfdecode segment: time = 0.38s, I/O = 77.71MiB/s    Selfdecode segment: time = 0.46s, I/O = 76.29MiB/s    Selfdecode segment: time = 0.55s, I/O = 74.47MiB/s    Selfdecode segment: time = 0.65s, I/O = 72.86MiB/s    Selfdecode segment: time = 0.74s, I/O = 71.60MiB/s    Selfdecode segment: time = 0.84s, I/O = 70.35MiB/s    Selfdecode segment: time = 0.94s, I/O = 69.47MiB/s    Selfdecode segment: time = 1.03s, I/O = 68.68MiB/s    Selfdecode segment: time = 1.14s, I/O = 67.67MiB/s    Selfdecode segment: time = 1.17s, I/O = 67.53MiB/s
    Write segment to disk: 0.08s, I/O = 2534.85MiB/s
    Decode the nearby phrases for next segment: 0.00s


Computation finished. Summary:
  I/O volume = 444294176 (2.12B/B)
  number of decoded phrases: 13971133
  average phrase length = 15.01
  length of decoded text: 209715200 (200.00MiB)
  elapsed time: 1.71s (0.0085s/MiB)
  speed: 117.05MiB/s
Decompression t(s)    : 2.13983917236
