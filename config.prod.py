# Config parameters used for the experiemnts reported in the paper. 
# To run the validation tests on smaller data, use config.ci.py
EMLZ_MEM_MB=8000
ref_sizes = [8, 400, 1000]
threads = 96
mem_limit_reference_methods = 500000
