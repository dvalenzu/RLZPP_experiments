#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail 

DATA_DIR="./data"
#DATA_DIR="/home/work/dvalenzu/data/large/"
for FILEPATH in ${DATA_DIR}/*; do
	FILENAME=$(basename -- "${FILEPATH}")
	if [[ ${FILENAME} == *.* ]]; then
		continue
	fi
	if [[ -d ${FILEPATH} ]]; then
		continue
	fi
  echo ""
  echo ""
  echo "*****"
	echo ${FILENAME}
  echo "*****"
  
  for K in 0 1 2 3 4 5 6 7; do
    ./data/tools/entrop2  ${FILEPATH}  ${K}
  done
  
  echo "*****"
  echo "LZ77 - style"
  echo "*****"
  ./ext/ReLZ/ReLZ -v4 -l 500 ${FILEPATH} -e ELIAS
  
  for REF in 10 50; do
    echo "*****"
    echo "ReLZ - ${REF}MB"
    echo "*****"
    ./ext/ReLZ/ReLZ -v4 -l ${REF} ${FILEPATH} -e ELIAS
  done
done
