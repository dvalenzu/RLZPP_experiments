RLZ parser intitialized with:
Ignore     : 0
Ref        : 189779940
Partitions : 32
Memory(MB) : 30000
RLZ parsing...
Building SA...
We will read blocks of size: 210866607
Reading block 0/1
Block read in 0.064002 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/1
Block read in 0.006798 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 9.77042 (s)
Writing phrases... 
Phrases written  in 0.027584 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 30000MB of  memory, the reference length will be: 1179648000 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 10541591
Ref        : 1593950
Partitions : 32
Memory(MB) : 30000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 12135541
Reading block 0/0
Block read in 0.004185 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 12
IM Random Access to sums file will use at most (MB): 17
 Random Access to Sum Files IM (delta compressed)./RESULTS_SMALL_REF/Sources/RLZ_PLUS_ref189779940_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: ./RESULTS_SMALL_REF/Sources/RLZ_PLUS_ref189779940_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 10541591
Factors RLZ          : 1593950
Factors Total        : 12135541
--------------------------------------------
Time SA          : 12.9407
Time LZ77 of ref : 5.31974
Time RLZ parse   : 9.80567
-------------------------------
Time RLZ total   : 28.0661
--------------------------------------
Rlz TOTAL time 	: 28.0997
Pack      time 	: 0.34609
Recursion time 	: 0.963721
ReParse   time 	: 0.729918
Encode    time 	: 1.69766
--------------------------------------
TOTAL     time 	: 31.8371

1-th level report: 

Factors skipped      : 10541591
Factors 77'ed in ref : 1262405
Factors RLZ          : 0
Factors Total        : 11803996
--------------------------------------------
Time SA          : 0.605066
Time LZ77 of ref : 0.110487
Time RLZ parse   : 1.00001e-06
-------------------------------
Time RLZ total   : 0.715554


Final Wtime(s)    : 31.8371
N Phrases         : 11803996
Compression ratio : 0.895656
seconds per MiB   : 0.158316
seconds per GiB   : 162.116
MiB per second    : 6.31647
GiB per second    : 0.00616843
	Command being timed: "./ext/RLZPP/rlz_plusplus /home/work/dvalenzu/data//Sources -o ./RESULTS_SMALL_REF/Sources/RLZ_PLUS_ref189779940_files/compressed_file -s -l 189779940 -t16 -c32 -m30000 -d1 -v4"
	User time (seconds): 59.23
	System time (seconds): 0.89
	Percent of CPU this job got: 188%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 0:31.84
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 1680456
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 533109
	Voluntary context switches: 440
	Involuntary context switches: 4813
	Swaps: 0
	File system inputs: 8
	File system outputs: 1118312
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
