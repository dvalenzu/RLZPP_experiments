RLZ parser intitialized with:
Ignore     : 0
Ref        : 2348409
Partitions : 32
Memory(MB) : 30000
RLZ parsing...
Building SA...
We will read blocks of size: 46968181
Reading block 0/1
Block read in 0.000853 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/1
Block read in 0.014626 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 0.961287 (s)
Writing phrases... 
Phrases written  in 0.035404 (s)
Input succesfully compressed, output written in: ./RESULTS_SMALL_REF/Leaders/RLZ_PRE_ref2348409_files/compressed_file
Parsing performed in 1 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 58853
Factors RLZ          : 2084164
Factors Total        : 2143017
--------------------------------------------
Time SA          : 0.079317
Time LZ77 of ref : 0.028564
Time RLZ parse   : 1.0114
-------------------------------
Time RLZ total   : 1.11928


Final Wtime(s)    : 1.23084
N Phrases         : 2143017
Compression ratio : 0.730032
seconds per MiB   : 0.0274788
seconds per GiB   : 28.1382
MiB per second    : 36.3917
GiB per second    : 0.0355388
	Command being timed: "./ext/RLZPP/rlz_plusplus /home/work/dvalenzu/data//Leaders -o ./RESULTS_SMALL_REF/Leaders/RLZ_PRE_ref2348409_files/compressed_file -s -l 2348409 -t16 -c32 -m30000 -d0 -v4"
	User time (seconds): 3.89
	System time (seconds): 0.12
	Percent of CPU this job got: 324%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 0:01.23
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 156808
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 41924
	Voluntary context switches: 418
	Involuntary context switches: 527
	Swaps: 0
	File system inputs: 0
	File system outputs: 84112
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
