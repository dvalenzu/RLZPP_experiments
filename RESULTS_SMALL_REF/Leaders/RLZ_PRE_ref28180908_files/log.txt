RLZ parser intitialized with:
Ignore     : 0
Ref        : 28180908
Partitions : 32
Memory(MB) : 30000
RLZ parsing...
Building SA...
We will read blocks of size: 46968181
Reading block 0/1
Block read in 0.010067 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/1
Block read in 0.006165 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 0.978474 (s)
Writing phrases... 
Phrases written  in 0.004756 (s)
Input succesfully compressed, output written in: ./RESULTS_SMALL_REF/Leaders/RLZ_PRE_ref28180908_files/compressed_file
Parsing performed in 1 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 144495
Factors RLZ          : 244499
Factors Total        : 388994
--------------------------------------------
Time SA          : 1.1561
Time LZ77 of ref : 0.469814
Time RLZ parse   : 0.989627
-------------------------------
Time RLZ total   : 2.61554


Final Wtime(s)    : 2.64326
N Phrases         : 388994
Compression ratio : 0.132513
seconds per MiB   : 0.0590115
seconds per GiB   : 60.4277
MiB per second    : 16.9459
GiB per second    : 0.0165487
	Command being timed: "./ext/RLZPP/rlz_plusplus /home/work/dvalenzu/data//Leaders -o ./RESULTS_SMALL_REF/Leaders/RLZ_PRE_ref28180908_files/compressed_file -s -l 28180908 -t16 -c32 -m30000 -d0 -v4"
	User time (seconds): 5.46
	System time (seconds): 0.08
	Percent of CPU this job got: 209%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 0:02.64
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 252520
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 70545
	Voluntary context switches: 91
	Involuntary context switches: 448
	Swaps: 0
	File system inputs: 0
	File system outputs: 15816
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
