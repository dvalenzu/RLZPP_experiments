RLZ parser intitialized with:
Ignore     : 0
Ref        : 37574544
Partitions : 32
Memory(MB) : 30000
RLZ parsing...
Building SA...
We will read blocks of size: 46968181
Reading block 0/1
Block read in 0.013029 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/1
Block read in 0.002909 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 0.35521 (s)
Writing phrases... 
Phrases written  in 0.001515 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 30000MB of  memory, the reference length will be: 1179648000 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 160530
Ref        : 76998
Partitions : 32
Memory(MB) : 30000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 237528
Reading block 0/0
Block read in 0.00017 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 17
IM Random Access to sums file will use at most (MB): 0
 Random Access to Sum Files IM (delta compressed)./RESULTS_SMALL_REF/Leaders/RLZ_PLUS_ref37574544_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: ./RESULTS_SMALL_REF/Leaders/RLZ_PLUS_ref37574544_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 160530
Factors RLZ          : 76998
Factors Total        : 237528
--------------------------------------------
Time SA          : 1.49966
Time LZ77 of ref : 0.646611
Time RLZ parse   : 0.360238
-------------------------------
Time RLZ total   : 2.50651
--------------------------------------
Rlz TOTAL time 	: 2.5144
Pack      time 	: 0.00879
Recursion time 	: 0.03468
ReParse   time 	: 0.016623
Encode    time 	: 0.009709
--------------------------------------
TOTAL     time 	: 2.58422

1-th level report: 

Factors skipped      : 160530
Factors 77'ed in ref : 18710
Factors RLZ          : 0
Factors Total        : 179240
--------------------------------------------
Time SA          : 0.029121
Time LZ77 of ref : 0.001209
Time RLZ parse   : 1.00001e-06
-------------------------------
Time RLZ total   : 0.030331


Final Wtime(s)    : 2.58422
N Phrases         : 179240
Compression ratio : 0.0610592
seconds per MiB   : 0.0576934
seconds per GiB   : 59.0781
MiB per second    : 17.333
GiB per second    : 0.0169267
	Command being timed: "./ext/RLZPP/rlz_plusplus /home/work/dvalenzu/data//Leaders -o ./RESULTS_SMALL_REF/Leaders/RLZ_PLUS_ref37574544_files/compressed_file -s -l 37574544 -t16 -c32 -m30000 -d1 -v4"
	User time (seconds): 3.48
	System time (seconds): 0.09
	Percent of CPU this job got: 138%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 0:02.58
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 334956
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 90535
	Voluntary context switches: 79
	Involuntary context switches: 162
	Swaps: 0
	File system inputs: 0
	File system outputs: 19032
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
