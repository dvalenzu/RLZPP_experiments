RLZ parser intitialized with:
Ignore     : 0
Ref        : 9393636
Partitions : 32
Memory(MB) : 30000
RLZ parsing...
Building SA...
We will read blocks of size: 46968181
Reading block 0/1
Block read in 0.009305 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/1
Block read in 0.012419 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 1.46387 (s)
Writing phrases... 
Phrases written  in 0.013894 (s)
Input succesfully compressed, output written in: ./RESULTS_SMALL_REF/Leaders/RLZ_PRE_ref9393636_files/compressed_file
Parsing performed in 1 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 112648
Factors RLZ          : 703413
Factors Total        : 816061
--------------------------------------------
Time SA          : 0.46592
Time LZ77 of ref : 0.149264
Time RLZ parse   : 1.4911
-------------------------------
Time RLZ total   : 2.10629


Final Wtime(s)    : 2.15331
N Phrases         : 816061
Compression ratio : 0.277996
seconds per MiB   : 0.0480731
seconds per GiB   : 49.2269
MiB per second    : 20.8016
GiB per second    : 0.0203141
	Command being timed: "./ext/RLZPP/rlz_plusplus /home/work/dvalenzu/data//Leaders -o ./RESULTS_SMALL_REF/Leaders/RLZ_PRE_ref9393636_files/compressed_file -s -l 9393636 -t16 -c32 -m30000 -d0 -v4"
	User time (seconds): 6.38
	System time (seconds): 0.06
	Percent of CPU this job got: 297%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 0:02.16
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 118940
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 40054
	Voluntary context switches: 218
	Involuntary context switches: 660
	Swaps: 0
	File system inputs: 0
	File system outputs: 33088
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
