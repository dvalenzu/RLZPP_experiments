RLZ parser intitialized with:
Ignore     : 0
Ref        : 62914560
Partitions : 32
Memory(MB) : 30000
RLZ parsing...
Building SA...
We will read blocks of size: 209715200
Reading block 0/1
Block read in 0.021823 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/1
Block read in 0.049785 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 81.6106 (s)
Writing phrases... 
Phrases written  in 0.239189 (s)
Input succesfully compressed, output written in: ./RESULTS_SMALL_REF/English/RLZ_PRE_ref62914560_files/compressed_file
Parsing performed in 1 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 4332571
Factors RLZ          : 13415097
Factors Total        : 17747668
--------------------------------------------
Time SA          : 5.52128
Time LZ77 of ref : 2.06799
Time RLZ parse   : 81.9067
-------------------------------
Time RLZ total   : 89.496


Final Wtime(s)    : 92.0021
N Phrases         : 17747668
Compression ratio : 1.35404
seconds per MiB   : 0.460011
seconds per GiB   : 471.051
MiB per second    : 2.17386
GiB per second    : 0.00212291
	Command being timed: "./ext/RLZPP/rlz_plusplus /home/work/dvalenzu/data//English -o ./RESULTS_SMALL_REF/English/RLZ_PRE_ref62914560_files/compressed_file -s -l 62914560 -t16 -c32 -m30000 -d0 -v4"
	User time (seconds): 333.38
	System time (seconds): 1.10
	Percent of CPU this job got: 363%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 1:32.01
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 1002664
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 348479
	Voluntary context switches: 530
	Involuntary context switches: 41136
	Swaps: 0
	File system inputs: 16
	File system outputs: 725088
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
