RLZ parser intitialized with:
Ignore     : 0
Ref        : 10485760
Partitions : 32
Memory(MB) : 30000
RLZ parsing...
Building SA...
We will read blocks of size: 209715200
Reading block 0/1
Block read in 0.003634 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/1
Block read in 0.068606 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 64.7534 (s)
Writing phrases... 
Phrases written  in 0.423434 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 30000MB of  memory, the reference length will be: 1179648000 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 776294
Ref        : 22620772
Partitions : 32
Memory(MB) : 30000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 23397066
Reading block 0/0
Block read in 0.058652 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 11
IM Random Access to sums file will use at most (MB): 30
 Random Access to Sum Files IM (delta compressed)./RESULTS_SMALL_REF/English/RLZ_PLUS_ref10485760_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: ./RESULTS_SMALL_REF/English/RLZ_PLUS_ref10485760_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 776294
Factors RLZ          : 22620772
Factors Total        : 23397066
--------------------------------------------
Time SA          : 0.707227
Time LZ77 of ref : 0.266621
Time RLZ parse   : 65.258
-------------------------------
Time RLZ total   : 66.2318
--------------------------------------
Rlz TOTAL time 	: 66.2423
Pack      time 	: 0.762331
Recursion time 	: 23.507
ReParse   time 	: 1.51424
Encode    time 	: 1.88267
--------------------------------------
TOTAL     time 	: 93.9086

1-th level report: 

Factors skipped      : 776294
Factors 77'ed in ref : 17069287
Factors RLZ          : 0
Factors Total        : 17845581
--------------------------------------------
Time SA          : 20.1708
Time LZ77 of ref : 3.30478
Time RLZ parse   : 1.00001e-06
-------------------------------
Time RLZ total   : 23.4756


Final Wtime(s)    : 93.9086
N Phrases         : 17845581
Compression ratio : 1.36151
seconds per MiB   : 0.469543
seconds per GiB   : 480.812
MiB per second    : 2.12973
GiB per second    : 0.00207981
	Command being timed: "./ext/RLZPP/rlz_plusplus /home/work/dvalenzu/data//English -o ./RESULTS_SMALL_REF/English/RLZ_PLUS_ref10485760_files/compressed_file -s -l 10485760 -t16 -c32 -m30000 -d1 -v4"
	User time (seconds): 284.78
	System time (seconds): 2.17
	Percent of CPU this job got: 305%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 1:33.92
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 1208560
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 697676
	Voluntary context switches: 926
	Involuntary context switches: 32884
	Swaps: 0
	File system inputs: 16
	File system outputs: 1842728
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
