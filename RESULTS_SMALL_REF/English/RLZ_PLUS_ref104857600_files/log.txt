RLZ parser intitialized with:
Ignore     : 0
Ref        : 104857600
Partitions : 32
Memory(MB) : 30000
RLZ parsing...
Building SA...
We will read blocks of size: 209715200
Reading block 0/1
Block read in 0.036477 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/1
Block read in 0.035149 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 63.7722 (s)
Writing phrases... 
Phrases written  in 0.154523 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 30000MB of  memory, the reference length will be: 1179648000 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 7408437
Ref        : 8887952
Partitions : 32
Memory(MB) : 30000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 16296389
Reading block 0/0
Block read in 0.023948 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 11
IM Random Access to sums file will use at most (MB): 21
 Random Access to Sum Files IM (delta compressed)./RESULTS_SMALL_REF/English/RLZ_PLUS_ref104857600_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: ./RESULTS_SMALL_REF/English/RLZ_PLUS_ref104857600_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 7408437
Factors RLZ          : 8887952
Factors Total        : 16296389
--------------------------------------------
Time SA          : 9.73382
Time LZ77 of ref : 3.63163
Time RLZ parse   : 63.9665
-------------------------------
Time RLZ total   : 77.3319
--------------------------------------
Rlz TOTAL time 	: 77.3547
Pack      time 	: 0.463849
Recursion time 	: 8.43433
ReParse   time 	: 0.967765
Encode    time 	: 2.12252
--------------------------------------
TOTAL     time 	: 89.3432

1-th level report: 

Factors skipped      : 7408437
Factors 77'ed in ref : 7086791
Factors RLZ          : 0
Factors Total        : 14495228
--------------------------------------------
Time SA          : 7.04942
Time LZ77 of ref : 1.19735
Time RLZ parse   : 1.00001e-06
-------------------------------
Time RLZ total   : 8.24677


Final Wtime(s)    : 89.3432
N Phrases         : 14495228
Compression ratio : 1.1059
seconds per MiB   : 0.446716
seconds per GiB   : 457.437
MiB per second    : 2.23856
GiB per second    : 0.00218609
	Command being timed: "./ext/RLZPP/rlz_plusplus /home/work/dvalenzu/data//English -o ./RESULTS_SMALL_REF/English/RLZ_PLUS_ref104857600_files/compressed_file -s -l 104857600 -t16 -c32 -m30000 -d1 -v4"
	User time (seconds): 277.39
	System time (seconds): 1.36
	Percent of CPU this job got: 311%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 1:29.35
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 1017688
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 561315
	Voluntary context switches: 634
	Involuntary context switches: 31851
	Swaps: 0
	File system inputs: 8
	File system outputs: 1425944
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
