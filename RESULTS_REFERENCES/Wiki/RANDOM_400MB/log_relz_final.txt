RLZ parser intitialized with:
Ignore     : 0
Ref        : 419430400
Partitions : 288
Memory(MB) : 500000
RLZ parsing...
Building SA...
We will read blocks of size: 2147483648
Reading block 0...Block read in 0.249103 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1...Block read in 1.19854 (s)
Waiting for all tasks to be done...
Done.
Reading block 2...Block read in 1.10402 (s)
Starting parallel parsing in block...Parallel parse in 49.8021 (s)
Writing phrases... 
Phrases writed  in 1.15623 (s)
Waiting for all tasks to be done...
Done.
Reading block 3...Block read in 0.966841 (s)
Starting parallel parsing in block...Parallel parse in 40.0357 (s)
Writing phrases... 
Phrases writed  in 1.84243 (s)
Waiting for all tasks to be done...
Done.
Reading block 4...Block read in 0.995987 (s)
Starting parallel parsing in block...Parallel parse in 45.3653 (s)
Writing phrases... 
Phrases writed  in 1.7122 (s)
Waiting for all tasks to be done...
Done.
Reading block 5...Block read in 1.11465 (s)
Starting parallel parsing in block...Parallel parse in 44.5584 (s)
Writing phrases... 
Phrases writed  in 1.58193 (s)
Waiting for all tasks to be done...
Done.
Reading block 6...Block read in 1.10386 (s)
Starting parallel parsing in block...Parallel parse in 43.6114 (s)
Writing phrases... 
Phrases writed  in 1.75634 (s)
Waiting for all tasks to be done...
Done.
Reading block 7...Block read in 0.975068 (s)
Starting parallel parsing in block...Parallel parse in 40.8701 (s)
Writing phrases... 
Phrases writed  in 1.65383 (s)
Waiting for all tasks to be done...
Done.
Reading block 8...Block read in 1.09857 (s)
Starting parallel parsing in block...Parallel parse in 40.8653 (s)
Writing phrases... 
Phrases writed  in 1.88457 (s)
Waiting for all tasks to be done...
Done.
Reading block 9...Block read in 1.04738 (s)
Starting parallel parsing in block...Parallel parse in 40.801 (s)
Writing phrases... 
Phrases writed  in 1.7024 (s)
Waiting for all tasks to be done...
Done.
Reading block 10...Block read in 1.07899 (s)
Starting parallel parsing in block...Parallel parse in 40.3511 (s)
Writing phrases... 
Phrases writed  in 1.76384 (s)
Waiting for all tasks to be done...
Done.
Reading block 11...Block read in 1.05433 (s)
Starting parallel parsing in block...Parallel parse in 45.6505 (s)
Writing phrases... 
Phrases writed  in 1.89426 (s)
Waiting for all tasks to be done...
Done.
Reading block 12...Block read in 0.94192 (s)
Starting parallel parsing in block...Parallel parse in 43.795 (s)
Writing phrases... 
Phrases writed  in 2.37125 (s)
Waiting for all tasks to be done...
Done.
Starting parallel parsing in block...Parallel parse in 42.7796 (s)
Writing phrases... 
Phrases writed  in 1.58553 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 500000MB of  memory, the reference length will be: 19660799999 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 8359036
Ref        : 574783622
Partitions : 288
Memory(MB) : 500000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 268435456
Reading block 0...Block read in 2.56061 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 13
IM Random Access to sums file will use at most (MB): 903
 Random Access to Sum Files IM (delta compressed)RESULTS_REFERENCES/WIKI/RANDOM_400MB/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: RESULTS_REFERENCES/WIKI/RANDOM_400MB/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 8359036
Factors RLZ          : 574783622
Factors Total        : 583142658
--------------------------------------------
Time SA          : 57.8945
Time LZ77 of ref : 14.0257
Time RLZ parse   : 552.147
-------------------------------
Time RLZ total   : 624.068
--------------------------------------
Rlz TOTAL time 	: 624.104
Pack      time 	: 31.722
Recursion time 	: 1120.45
ReParse   time 	: 82.2295
Encode    time 	: 0
--------------------------------------
TOTAL     time 	: 1858.5

1-th level report: 

Factors skipped      : 8359036
Factors 77'ed in ref : 372960383
Factors RLZ          : 0
Factors Total        : 381319419
--------------------------------------------
Time SA          : 959.037
Time LZ77 of ref : 160.998
Time RLZ parse   : 2.00002e-06
-------------------------------
Time RLZ total   : 1120.04


Final Wtime(s)    : 1858.5
N Phrases         : 381319419
Compression ratio : 0.0812238
seconds per MiB   : 0.0750837
seconds per GiB   : 76.8857
MiB per second    : 13.3185
GiB per second    : 0.0130063
