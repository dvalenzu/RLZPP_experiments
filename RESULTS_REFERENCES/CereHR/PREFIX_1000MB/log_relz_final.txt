RLZ parser intitialized with:
Ignore     : 0
Ref        : 1048576000
Partitions : 288
Memory(MB) : 500000
RLZ parsing...
Building SA...
We will read blocks of size: 2147483648
Reading block 0...Block read in 1.03922 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1...Block read in 1.98289 (s)
Waiting for all tasks to be done...
Done.
Reading block 2...Block read in 1.95308 (s)
Starting parallel parsing in block...Parallel parse in 4.26774 (s)
Writing phrases... 
Phrases writed  in 0.07996 (s)
Waiting for all tasks to be done...
Done.
Reading block 3...Block read in 1.54356 (s)
Starting parallel parsing in block...Parallel parse in 7.72904 (s)
Writing phrases... 
Phrases writed  in 0.15669 (s)
Waiting for all tasks to be done...
Done.
Reading block 4...Block read in 1.8025 (s)
Starting parallel parsing in block...Parallel parse in 8.39709 (s)
Writing phrases... 
Phrases writed  in 0.217317 (s)
Waiting for all tasks to be done...
Done.
Reading block 5...Block read in 1.59162 (s)
Starting parallel parsing in block...Parallel parse in 9.73464 (s)
Writing phrases... 
Phrases writed  in 0.306294 (s)
Waiting for all tasks to be done...
Done.
Reading block 6...Block read in 1.91287 (s)
Starting parallel parsing in block...Parallel parse in 10.8407 (s)
Writing phrases... 
Phrases writed  in 0.369387 (s)
Waiting for all tasks to be done...
Done.
Reading block 7...Block read in 1.60441 (s)
Starting parallel parsing in block...Parallel parse in 12.1128 (s)
Writing phrases... 
Phrases writed  in 0.49541 (s)
Waiting for all tasks to be done...
Done.
Reading block 8...Block read in 1.95256 (s)
Starting parallel parsing in block...Parallel parse in 13.2423 (s)
Writing phrases... 
Phrases writed  in 0.560992 (s)
Waiting for all tasks to be done...
Done.
Reading block 9...Block read in 1.6052 (s)
Starting parallel parsing in block...Parallel parse in 14.3675 (s)
Writing phrases... 
Phrases writed  in 0.586719 (s)
Waiting for all tasks to be done...
Done.
Reading block 10...Block read in 1.72635 (s)
Starting parallel parsing in block...Parallel parse in 15.8329 (s)
Writing phrases... 
Phrases writed  in 0.625514 (s)
Waiting for all tasks to be done...
Done.
Reading block 11...Block read in 0.714581 (s)
Starting parallel parsing in block...Parallel parse in 17.6875 (s)
Writing phrases... 
Phrases writed  in 0.684766 (s)
Waiting for all tasks to be done...
Done.
Starting parallel parsing in block...Parallel parse in 8.70019 (s)
Writing phrases... 
Phrases writed  in 0.353754 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 500000MB of  memory, the reference length will be: 19660799999 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 1817998
Ref        : 101293752
Partitions : 288
Memory(MB) : 500000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 103111750
Reading block 0...Block read in 0.675975 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 17
IM Random Access to sums file will use at most (MB): 208
 Random Access to Sum Files IM (delta compressed)RESULTS_REFERENCES/CEREHR/PREFIX_1000MB/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: RESULTS_REFERENCES/CEREHR/PREFIX_1000MB/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 1817998
Factors RLZ          : 101293752
Factors Total        : 103111750
--------------------------------------------
Time SA          : 233.65
Time LZ77 of ref : 51.7288
Time RLZ parse   : 145.965
-------------------------------
Time RLZ total   : 431.344
--------------------------------------
Rlz TOTAL time 	: 432.566
Pack      time 	: 5.83468
Recursion time 	: 192.999
ReParse   time 	: 9.79141
Encode    time 	: 0
--------------------------------------
TOTAL     time 	: 641.192

1-th level report: 

Factors skipped      : 1817998
Factors 77'ed in ref : 8390498
Factors RLZ          : 0
Factors Total        : 10208496
--------------------------------------------
Time SA          : 183.91
Time LZ77 of ref : 8.79657
Time RLZ parse   : 3.00002e-06
-------------------------------
Time RLZ total   : 192.706


Final Wtime(s)    : 641.192
N Phrases         : 10208496
Compression ratio : 0.00271655
seconds per MiB   : 0.028579
seconds per GiB   : 29.2649
MiB per second    : 34.9907
GiB per second    : 0.0341706
