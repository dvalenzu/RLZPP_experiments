RLZ parser intitialized with:
Ignore     : 0
Ref        : 8388608
Partitions : 288
Memory(MB) : 500000
RLZ parsing...
Building SA...
We will read blocks of size: 2147483648
Reading block 0...Block read in 0.014469 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1...Block read in 3.43123 (s)
Waiting for all tasks to be done...
Done.
Reading block 2...Block read in 3.30294 (s)
Starting parallel parsing in block...Parallel parse in 9.58025 (s)
Writing phrases... 
Phrases writed  in 8.47686 (s)
Waiting for all tasks to be done...
Done.
Reading block 3...Block read in 3.73013 (s)
Starting parallel parsing in block...Parallel parse in 10.1857 (s)
Writing phrases... 
Phrases writed  in 8.9589 (s)
Waiting for all tasks to be done...
Done.
Reading block 4...Block read in 3.33707 (s)
Starting parallel parsing in block...Parallel parse in 10.0156 (s)
Writing phrases... 
Phrases writed  in 7.33967 (s)
Waiting for all tasks to be done...
Done.
Reading block 5...Block read in 3.30055 (s)
Starting parallel parsing in block...Parallel parse in 10.0442 (s)
Writing phrases... 
Phrases writed  in 7.95629 (s)
Waiting for all tasks to be done...
Done.
Reading block 6...Block read in 3.34237 (s)
Starting parallel parsing in block...Parallel parse in 9.78759 (s)
Writing phrases... 
Phrases writed  in 7.5858 (s)
Waiting for all tasks to be done...
Done.
Reading block 7...Block read in 3.33891 (s)
Starting parallel parsing in block...Parallel parse in 10.6139 (s)
Writing phrases... 
Phrases writed  in 8.03128 (s)
Waiting for all tasks to be done...
Done.
Reading block 8...Block read in 3.57986 (s)
Starting parallel parsing in block...Parallel parse in 10.0766 (s)
Writing phrases... 
Phrases writed  in 7.30346 (s)
Waiting for all tasks to be done...
Done.
Reading block 9...Block read in 3.33907 (s)
Starting parallel parsing in block...Parallel parse in 9.8818 (s)
Writing phrases... 
Phrases writed  in 7.25401 (s)
Waiting for all tasks to be done...
Done.
Reading block 10...Block read in 3.32321 (s)
Starting parallel parsing in block...Parallel parse in 9.67551 (s)
Writing phrases... 
Phrases writed  in 7.2655 (s)
Waiting for all tasks to be done...
Done.
Reading block 11...Block read in 3.19635 (s)
Starting parallel parsing in block...Parallel parse in 10.0402 (s)
Writing phrases... 
Phrases writed  in 7.19299 (s)
Waiting for all tasks to be done...
Done.
Reading block 12...Block read in 3.29051 (s)
Starting parallel parsing in block...Parallel parse in 9.99688 (s)
Writing phrases... 
Phrases writed  in 7.11767 (s)
Waiting for all tasks to be done...
Done.
Reading block 13...Block read in 3.29628 (s)
Starting parallel parsing in block...Parallel parse in 10.518 (s)
Writing phrases... 
Phrases writed  in 7.19863 (s)
Waiting for all tasks to be done...
Done.
Reading block 14...Block read in 3.35343 (s)
Starting parallel parsing in block...Parallel parse in 9.89391 (s)
Writing phrases... 
Phrases writed  in 7.13563 (s)
Waiting for all tasks to be done...
Done.
Reading block 15...Block read in 3.32638 (s)
Starting parallel parsing in block...Parallel parse in 10.5675 (s)
Writing phrases... 
Phrases writed  in 7.18965 (s)
Waiting for all tasks to be done...
Done.
Reading block 16...Block read in 3.25456 (s)
Starting parallel parsing in block...Parallel parse in 10.126 (s)
Writing phrases... 
Phrases writed  in 7.23466 (s)
Waiting for all tasks to be done...
Done.
Reading block 17...Block read in 3.29109 (s)
Starting parallel parsing in block...Parallel parse in 10.0202 (s)
Writing phrases... 
Phrases writed  in 7.11097 (s)
Waiting for all tasks to be done...
Done.
Reading block 18...Block read in 3.26582 (s)
Starting parallel parsing in block...Parallel parse in 9.87587 (s)
Writing phrases... 
Phrases writed  in 7.16022 (s)
Waiting for all tasks to be done...
Done.
Reading block 19...Block read in 3.26102 (s)
Starting parallel parsing in block...Parallel parse in 10.5962 (s)
Writing phrases... 
Phrases writed  in 7.10459 (s)
Waiting for all tasks to be done...
Done.
Reading block 20...Block read in 3.35285 (s)
Starting parallel parsing in block...Parallel parse in 11.4526 (s)
Writing phrases... 
Phrases writed  in 6.96431 (s)
Waiting for all tasks to be done...
Done.
Reading block 21...Block read in 3.25554 (s)
Starting parallel parsing in block...Parallel parse in 10.8913 (s)
Writing phrases... 
Phrases writed  in 7.37747 (s)
Waiting for all tasks to be done...
Done.
Reading block 22...Block read in 3.58945 (s)
Starting parallel parsing in block...Parallel parse in 9.94038 (s)
Writing phrases... 
Phrases writed  in 7.63807 (s)
Waiting for all tasks to be done...
Done.
Reading block 23...Block read in 3.27664 (s)
Starting parallel parsing in block...Parallel parse in 11.1904 (s)
Writing phrases... 
Phrases writed  in 7.70893 (s)
Waiting for all tasks to be done...
Done.
Reading block 24...Block read in 4.01553 (s)
Starting parallel parsing in block...Parallel parse in 10.3411 (s)
Writing phrases... 
Phrases writed  in 6.99916 (s)
Waiting for all tasks to be done...
Done.
Reading block 25...Block read in 3.31526 (s)
Starting parallel parsing in block...Parallel parse in 9.70635 (s)
Writing phrases... 
Phrases writed  in 6.9959 (s)
Waiting for all tasks to be done...
Done.
Reading block 26...Block read in 3.33848 (s)
Starting parallel parsing in block...Parallel parse in 9.84804 (s)
Writing phrases... 
Phrases writed  in 7.4734 (s)
Waiting for all tasks to be done...
Done.
Reading block 27...Block read in 3.27988 (s)
Starting parallel parsing in block...Parallel parse in 9.96414 (s)
Writing phrases... 
Phrases writed  in 6.95787 (s)
Waiting for all tasks to be done...
Done.
Reading block 28...Block read in 3.34231 (s)
Starting parallel parsing in block...Parallel parse in 10.2488 (s)
Writing phrases... 
Phrases writed  in 6.41636 (s)
Waiting for all tasks to be done...
Done.
Reading block 29...Block read in 3.87761 (s)
Starting parallel parsing in block...Parallel parse in 9.93797 (s)
Writing phrases... 
Phrases writed  in 6.88074 (s)
Waiting for all tasks to be done...
Done.
Reading block 30...Block read in 3.34091 (s)
Starting parallel parsing in block...Parallel parse in 9.71548 (s)
Writing phrases... 
Phrases writed  in 6.30426 (s)
Waiting for all tasks to be done...
Done.
Reading block 31...Block read in 3.14926 (s)
Starting parallel parsing in block...Parallel parse in 10.0375 (s)
Writing phrases... 
Phrases writed  in 7.001 (s)
Waiting for all tasks to be done...
Done.
Reading block 32...Block read in 3.24318 (s)
Starting parallel parsing in block...Parallel parse in 10.1273 (s)
Writing phrases... 
Phrases writed  in 7.15888 (s)
Waiting for all tasks to be done...
Done.
Reading block 33...Block read in 2.9e-05 (s)
Starting parallel parsing in block...Parallel parse in 10.292 (s)
Writing phrases... 
Phrases writed  in 6.75758 (s)
Waiting for all tasks to be done...
Done.
Starting parallel parsing in block...Parallel parse in 0.007818 (s)
Writing phrases... 
Phrases writed  in 1.6e-05 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 500000MB of  memory, the reference length will be: 19660799999 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 813911
Ref        : 7179716547
Partitions : 288
Memory(MB) : 500000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 268435456
Reading block 0...Block read in 41.0756 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 11
IM Random Access to sums file will use at most (MB): 9415
 Random Access to Sum Files IM (delta compressed)RESULTS_REFERENCES/KERNEL/PRUNE_8MB/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: RESULTS_REFERENCES/KERNEL/PRUNE_8MB/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 813911
Factors RLZ          : 7179716547
Factors Total        : 7180530458
--------------------------------------------
Time SA          : 0.599165
Time LZ77 of ref : 0.190983
Time RLZ parse   : 676.381
-------------------------------
Time RLZ total   : 677.171
--------------------------------------
Rlz TOTAL time 	: 677.605
Pack      time 	: 342.902
Recursion time 	: 45690.8
ReParse   time 	: 458.659
Encode    time 	: 1.00001e-06
--------------------------------------
TOTAL     time 	: 47170

1-th level report: 

Factors skipped      : 813911
Factors 77'ed in ref : 40003187
Factors RLZ          : 0
Factors Total        : 40817098
--------------------------------------------
Time SA          : 45014.6
Time LZ77 of ref : 668.679
Time RLZ parse   : 1.1e-05
-------------------------------
Time RLZ total   : 45683.3


Final Wtime(s)    : 47170
N Phrases         : 40817098
Compression ratio : 0.00291488
seconds per MiB   : 0.719669
seconds per GiB   : 736.941
MiB per second    : 1.38953
GiB per second    : 0.00135696
