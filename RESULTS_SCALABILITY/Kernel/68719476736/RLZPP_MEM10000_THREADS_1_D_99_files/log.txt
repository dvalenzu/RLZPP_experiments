
--- computing reference lenght based on memory limit:
--- With 10000MB of  memory, the reference length will be: 1048575999 symbols
RLZ parser intitialized with:
Ignore     : 0
Ref        : 1048575999
Partitions : 3
Memory(MB) : 10000
RLZ parsing...
Building SA...
We will read blocks of size: 1835008001
Reading block 0...Block read in 7.73925 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1...Block read in 13.4174 (s)
Waiting for all tasks to be done...
Done.
Reading block 2...Block read in 13.4593 (s)
Starting parallel parsing in block...Parallel parse in 26.07 (s)
Writing phrases... 
Phrases writed  in 0.027436 (s)
Waiting for all tasks to be done...
Done.
Reading block 3...Block read in 13.3795 (s)
Starting parallel parsing in block...Parallel parse in 26.0288 (s)
Writing phrases... 
Phrases writed  in 0.025797 (s)
Waiting for all tasks to be done...
Done.
Reading block 4...Block read in 13.4065 (s)
Starting parallel parsing in block...Parallel parse in 68.4278 (s)
Writing phrases... 
Phrases writed  in 0.084138 (s)
Waiting for all tasks to be done...
Done.
Reading block 5...Block read in 13.4857 (s)
Starting parallel parsing in block...Parallel parse in 81.586 (s)
Writing phrases... 
Phrases writed  in 0.100366 (s)
Waiting for all tasks to be done...
Done.
Reading block 6...Block read in 13.516 (s)
Starting parallel parsing in block...Parallel parse in 98.1104 (s)
Writing phrases... 
Phrases writed  in 0.125936 (s)
Waiting for all tasks to be done...
Done.
Reading block 7...Block read in 13.4873 (s)
Starting parallel parsing in block...Parallel parse in 99.7886 (s)
Writing phrases... 
Phrases writed  in 0.127332 (s)
Waiting for all tasks to be done...
Done.
Reading block 8...Block read in 13.4381 (s)
Starting parallel parsing in block...Parallel parse in 95.0025 (s)
Writing phrases... 
Phrases writed  in 0.118925 (s)
Waiting for all tasks to be done...
Done.
Reading block 9...Block read in 13.4334 (s)
Starting parallel parsing in block...Parallel parse in 115.87 (s)
Writing phrases... 
Phrases writed  in 0.155232 (s)
Waiting for all tasks to be done...
Done.
Reading block 10...Block read in 13.4984 (s)
Starting parallel parsing in block...Parallel parse in 120.326 (s)
Writing phrases... 
Phrases writed  in 0.162302 (s)
Waiting for all tasks to be done...
Done.
Reading block 11...Block read in 13.4375 (s)
Starting parallel parsing in block...Parallel parse in 120.468 (s)
Writing phrases... 
Phrases writed  in 0.16108 (s)
Waiting for all tasks to be done...
Done.
Reading block 12...Block read in 13.5289 (s)
Starting parallel parsing in block...Parallel parse in 117.568 (s)
Writing phrases... 
Phrases writed  in 0.15729 (s)
Waiting for all tasks to be done...
Done.
Reading block 13...Block read in 13.5561 (s)
Starting parallel parsing in block...Parallel parse in 110.604 (s)
Writing phrases... 
Phrases writed  in 0.147382 (s)
Waiting for all tasks to be done...
Done.
Reading block 14...Block read in 13.5585 (s)
Starting parallel parsing in block...Parallel parse in 119.983 (s)
Writing phrases... 
Phrases writed  in 0.16061 (s)
Waiting for all tasks to be done...
Done.
Reading block 15...Block read in 13.5927 (s)
Starting parallel parsing in block...Parallel parse in 121.523 (s)
Writing phrases... 
Phrases writed  in 0.164075 (s)
Waiting for all tasks to be done...
Done.
Reading block 16...Block read in 13.5155 (s)
Starting parallel parsing in block...Parallel parse in 127.713 (s)
Writing phrases... 
Phrases writed  in 0.17074 (s)
Waiting for all tasks to be done...
Done.
Reading block 17...Block read in 13.5008 (s)
Starting parallel parsing in block...Parallel parse in 135.932 (s)
Writing phrases... 
Phrases writed  in 0.179196 (s)
Waiting for all tasks to be done...
Done.
Reading block 18...Block read in 13.5605 (s)
Starting parallel parsing in block...Parallel parse in 136.485 (s)
Writing phrases... 
Phrases writed  in 0.179522 (s)
Waiting for all tasks to be done...
Done.
Reading block 19...Block read in 13.5119 (s)
Starting parallel parsing in block...Parallel parse in 137.26 (s)
Writing phrases... 
Phrases writed  in 0.180001 (s)
Waiting for all tasks to be done...
Done.
Reading block 20...Block read in 13.5172 (s)
Starting parallel parsing in block...Parallel parse in 142.929 (s)
Writing phrases... 
Phrases writed  in 0.188363 (s)
Waiting for all tasks to be done...
Done.
Reading block 21...Block read in 13.4654 (s)
Starting parallel parsing in block...Parallel parse in 150.47 (s)
Writing phrases... 
Phrases writed  in 0.198917 (s)
Waiting for all tasks to be done...
Done.
Reading block 22...Block read in 13.5797 (s)
Starting parallel parsing in block...Parallel parse in 151.1 (s)
Writing phrases... 
Phrases writed  in 0.199242 (s)
Waiting for all tasks to be done...
Done.
Reading block 23...Block read in 13.5865 (s)
Starting parallel parsing in block...Parallel parse in 150.472 (s)
Writing phrases... 
Phrases writed  in 0.198875 (s)
Waiting for all tasks to be done...
Done.
Reading block 24...Block read in 13.5979 (s)
Starting parallel parsing in block...Parallel parse in 150.811 (s)
Writing phrases... 
Phrases writed  in 0.198821 (s)
Waiting for all tasks to be done...
Done.
Reading block 25...Block read in 13.5229 (s)
Starting parallel parsing in block...Parallel parse in 151.924 (s)
Writing phrases... 
Phrases writed  in 0.199973 (s)
Waiting for all tasks to be done...
Done.
Reading block 26...Block read in 14.4066 (s)
Starting parallel parsing in block...Parallel parse in 151.775 (s)
Writing phrases... 
Phrases writed  in 0.200747 (s)
Waiting for all tasks to be done...
Done.
Reading block 27...Block read in 13.6063 (s)
Starting parallel parsing in block...Parallel parse in 152.176 (s)
Writing phrases... 
Phrases writed  in 0.199662 (s)
Waiting for all tasks to be done...
Done.
Reading block 28...Block read in 13.685 (s)
Starting parallel parsing in block...Parallel parse in 155.33 (s)
Writing phrases... 
Phrases writed  in 0.205435 (s)
Waiting for all tasks to be done...
Done.
Reading block 29...Block read in 13.6636 (s)
Starting parallel parsing in block...Parallel parse in 167.483 (s)
Writing phrases... 
Phrases writed  in 0.220494 (s)
Waiting for all tasks to be done...
Done.
Reading block 30...Block read in 13.712 (s)
Starting parallel parsing in block...Parallel parse in 167.588 (s)
Writing phrases... 
Phrases writed  in 0.222184 (s)
Waiting for all tasks to be done...
Done.
Reading block 31...Block read in 13.7885 (s)
Starting parallel parsing in block...Parallel parse in 180.306 (s)
Writing phrases... 
Phrases writed  in 0.242 (s)
Waiting for all tasks to be done...
Done.
Reading block 32...Block read in 13.7614 (s)
Starting parallel parsing in block...Parallel parse in 192.793 (s)
Writing phrases... 
Phrases writed  in 0.265684 (s)
Waiting for all tasks to be done...
Done.
Reading block 33...Block read in 13.7314 (s)
Starting parallel parsing in block...Parallel parse in 194.167 (s)
Writing phrases... 
Phrases writed  in 0.263427 (s)
Waiting for all tasks to be done...
Done.
Reading block 34...Block read in 13.7971 (s)
Starting parallel parsing in block...Parallel parse in 194.984 (s)
Writing phrases... 
Phrases writed  in 0.262484 (s)
Waiting for all tasks to be done...
Done.
Reading block 35...Block read in 13.7692 (s)
Starting parallel parsing in block...Parallel parse in 196.19 (s)
Writing phrases... 
Phrases writed  in 0.267564 (s)
Waiting for all tasks to be done...
Done.
Reading block 36...Block read in 13.7692 (s)
Starting parallel parsing in block...Parallel parse in 194.413 (s)
Writing phrases... 
Phrases writed  in 0.261376 (s)
Waiting for all tasks to be done...
Done.
Reading block 37...Block read in 12.0838 (s)
Starting parallel parsing in block...Parallel parse in 199.828 (s)
Writing phrases... 
Phrases writed  in 0.269457 (s)
Waiting for all tasks to be done...
Done.
Starting parallel parsing in block...Parallel parse in 173.022 (s)
Writing phrases... 
Phrases writed  in 0.232767 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 10000MB of  memory, the reference length will be: 393215999 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 25460592
Ref        : 327261166
Partitions : 3
Memory(MB) : 10000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 229669183
Reading block 0...Block read in 0.786803 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 17
IM Random Access to sums file will use at most (MB): 714
 Random Access to Sum Files IM (delta compressed)KERNEL/68719476736/RLZPP_MEM10000_THREADS_1_D_99_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: KERNEL/68719476736/RLZPP_MEM10000_THREADS_1_D_99_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 25460592
Factors RLZ          : 327261166
Factors Total        : 352721758
--------------------------------------------
Time SA          : 134.637
Time LZ77 of ref : 29.4169
Time RLZ parse   : 5584.81
-------------------------------
Time RLZ total   : 5748.86
--------------------------------------
Rlz TOTAL time 	: 5749.13
Pack      time 	: 11.6552
Recursion time 	: 602.89
ReParse   time 	: 15.133
Encode    time 	: 1.73783
--------------------------------------
TOTAL     time 	: 6380.54

1-th level report: 

Factors skipped      : 25460592
Factors 77'ed in ref : 3412865
Factors RLZ          : 0
Factors Total        : 28873457
--------------------------------------------
Time SA          : 593.706
Time LZ77 of ref : 8.44463
Time RLZ parse   : 2.00002e-06
-------------------------------
Time RLZ total   : 602.15


Final Wtime(s)    : 6380.54
N Phrases         : 28873457
Compression ratio : 0.00672263
seconds per MiB   : 0.0973594
seconds per GiB   : 99.696
MiB per second    : 10.2712
GiB per second    : 0.0100305
