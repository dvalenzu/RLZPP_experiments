
--- computing reference lenght based on memory limit:
--- With 4000MB of  memory, the reference length will be: 419430399 symbols
RLZ parser intitialized with:
Ignore     : 0
Ref        : 419430399
Partitions : 3
Memory(MB) : 4000
RLZ parsing...
Building SA...
We will read blocks of size: 734003201
Reading block 0/3
Block read in 0.138624 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/3
Block read in 0.239501 (s)
Waiting for all tasks to be done...
Done.
Reading block 2/3
Block read in 0.23742 (s)

*
Starting parallel parsing in block...Parallel parse in 148.027 (s)
Writing phrases... 
Phrases written  in 0.260004 (s)
Waiting for all tasks to be done...
Done.
Reading block 3/3
Block read in 0.046852 (s)

*
Starting parallel parsing in block...Parallel parse in 79.4366 (s)
Writing phrases... 
Phrases written  in 0.136532 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 72.8601 (s)
Writing phrases... 
Phrases written  in 0.128006 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 4000MB of  memory, the reference length will be: 157286399 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 21709237
Ref        : 27399888
Partitions : 3
Memory(MB) : 4000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 49109125
Reading block 0/0
Block read in 0.066805 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 13
IM Random Access to sums file will use at most (MB): 76
 Random Access to Sum Files IM (delta compressed)RESULTS_SCALABILITY/KERNEL//2147483648/RLZPP_MEM4000_THREADS_1_D_99_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: RESULTS_SCALABILITY/KERNEL//2147483648/RLZPP_MEM4000_THREADS_1_D_99_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 21709237
Factors RLZ          : 27399888
Factors Total        : 49109125
--------------------------------------------
Time SA          : 30.9955
Time LZ77 of ref : 13.0687
Time RLZ parse   : 301.382
-------------------------------
Time RLZ total   : 345.447
--------------------------------------
Rlz TOTAL time 	: 345.535
Pack      time 	: 1.59178
Recursion time 	: 60.9365
ReParse   time 	: 2.72575
Encode    time 	: 0
--------------------------------------
TOTAL     time 	: 410.789

1-th level report: 

Factors skipped      : 21709237
Factors 77'ed in ref : 5127256
Factors RLZ          : 0
Factors Total        : 26836493
--------------------------------------------
Time SA          : 59.2343
Time LZ77 of ref : 1.13805
Time RLZ parse   : 1.00001e-06
-------------------------------
Time RLZ total   : 60.3724


Final Wtime(s)    : 410.789
N Phrases         : 26836493
Compression ratio : 0.065389
seconds per MiB   : 0.200581
seconds per GiB   : 205.395
MiB per second    : 4.98553
GiB per second    : 0.00486868
Input filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/KERNEL/2147483648/RLZPP_MEM4000_THREADS_1_D_99_files/compressed_file
Output filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/KERNEL/2147483648/RLZPP_MEM4000_THREADS_1_D_99_files/compressed_file.decompressed
Input size = 140421907 (133.9MiB)
Max disk use = 1152921504606846976 (1099511627776.0MiB)
RAM use = 3758096384 (3584.0MiB)
RAM use (excl. buffer) = 3724541952 (3552.0MiB)
Max segment size = 3724541952 (3552.0MiB)

Process part 1:
  Total parsing data decoded so far: 0.00%
  Total text decoded so far: 0 (0.00MiB)
  Permute phrases by source:   Permute phrases by source: time = 0.12s, I/O = 90.49MiB/s  Permute phrases by source: time = 0.21s, I/O = 103.22MiB/s  Permute phrases by source: time = 0.25s, I/O = 131.19MiB/s  Permute phrases by source: time = 0.28s, I/O = 153.92MiB/s  Permute phrases by source: time = 0.32s, I/O = 171.48MiB/s  Permute phrases by source: time = 0.35s, I/O = 185.60MiB/s  Permute phrases by source: time = 0.39s, I/O = 197.24MiB/s  Permute phrases by source: time = 0.42s, I/O = 207.00MiB/s  Permute phrases by source: time = 0.46s, I/O = 215.19MiB/s  Permute phrases by source: time = 0.49s, I/O = 222.23MiB/s  Permute phrases by source: time = 0.53s, I/O = 228.39MiB/s  Permute phrases by source: time = 0.56s, I/O = 233.78MiB/s  Permute phrases by source: time = 0.60s, I/O = 238.58MiB/s  Permute phrases by source: time = 0.63s, I/O = 242.69MiB/s  Permute phrases by source: time = 0.67s, I/O = 246.31MiB/s  Permute phrases by source: time = 0.71s, I/O = 249.71MiB/s  Permute phrases by source: time = 0.75s, I/O = 252.63MiB/s  Permute phrases by source: time = 0.79s, I/O = 255.42MiB/s  Permute phrases by source: time = 0.83s, I/O = 257.99MiB/s  Permute phrases by source: time = 0.87s, I/O = 260.43MiB/s  Permute phrases by source: time = 0.91s, I/O = 262.43MiB/s  Permute phrases by source: time = 0.95s, I/O = 263.96MiB/s  Permute phrases by source: time = 0.99s, I/O = 265.40MiB/s  Permute phrases by source: time = 1.03s, I/O = 266.67MiB/s  Permute phrases by source: time = 1.07s, I/O = 267.88MiB/s  Permute phrases by source: 100.0%, time = 1.09s, I/O = 268.57MiB/s
  Processing this part will decode 2048.0MiB of text (100.00% of parsing)
  Last part = TRUE
  Decode segment 1/1 [0..2147483648):
    Selfdecode segment:     Selfdecode segment: time = 0.04s, I/O = 136.48MiB/s    Selfdecode segment: time = 0.10s, I/O = 117.61MiB/s    Selfdecode segment: time = 0.16s, I/O = 108.30MiB/s    Selfdecode segment: time = 0.23s, I/O = 104.65MiB/s    Selfdecode segment: time = 0.29s, I/O = 101.16MiB/s    Selfdecode segment: time = 0.36s, I/O = 98.11MiB/s    Selfdecode segment: time = 0.43s, I/O = 96.13MiB/s    Selfdecode segment: time = 0.50s, I/O = 95.63MiB/s    Selfdecode segment: time = 0.56s, I/O = 95.04MiB/s    Selfdecode segment: time = 0.64s, I/O = 93.56MiB/s    Selfdecode segment: time = 0.71s, I/O = 92.50MiB/s    Selfdecode segment: time = 0.78s, I/O = 92.17MiB/s    Selfdecode segment: time = 0.85s, I/O = 91.35MiB/s    Selfdecode segment: time = 0.92s, I/O = 90.67MiB/s    Selfdecode segment: time = 1.00s, I/O = 90.32MiB/s    Selfdecode segment: time = 1.08s, I/O = 89.95MiB/s    Selfdecode segment: time = 1.15s, I/O = 89.59MiB/s    Selfdecode segment: time = 1.23s, I/O = 89.30MiB/s    Selfdecode segment: time = 1.31s, I/O = 89.06MiB/s    Selfdecode segment: time = 1.39s, I/O = 88.70MiB/s    Selfdecode segment: time = 1.47s, I/O = 88.28MiB/s    Selfdecode segment: time = 1.55s, I/O = 88.18MiB/s    Selfdecode segment: time = 1.62s, I/O = 87.97MiB/s    Selfdecode segment: time = 1.70s, I/O = 87.71MiB/s    Selfdecode segment: time = 1.78s, I/O = 87.44MiB/s    Selfdecode segment: time = 2.30s, I/O = 69.47MiB/s
    Write segment to disk: 4.87s, I/O = 420.65MiB/s
    Decode the nearby phrases for next segment: 0.00s


Computation finished. Summary:
  I/O volume = 2622438637 (1.22B/B)
  number of decoded phrases: 26836493
  average phrase length = 80.02
  length of decoded text: 2147483648 (2048.00MiB)
  elapsed time: 9.39s (0.0046s/MiB)
  speed: 218.20MiB/s
Decompression t(s)    : 9.55995106697
