LZ77 Parsing ...
Final Wtime(s)    : 14453
N Phrases         : 24063400
OnlineLz77ViaRlbwt object (0x7fff46fd5d80) printStatistics(0) BEGIN
Len with endmarker = 8589934593
emPos_ = 228518000, succSamplePos_ = 7599173580, em_ = 0
DynRleForRlbwt object (0x7fff46fd5d80) printStatistics(0) BEGIN
TotalLen = 8589934592, #Runs = 112958748, Alphabet Size = 229
BTreeNode arity kB = 32, BtmNode arity kBtmBM = 32, BtmNode arity kBtmBS = 8
MTree bottom array size = 4262018, capacity = 4262400
STree bottom array size = 17172452, capacity = 17172480
Total: 2569453720 bytes = 2.50923e+06 KiB = 2450.42 MiB
MTree: 89131680 bytes, OccuRate = 84.414 (= 100*4425862/5243040)
ATree: 5440 bytes, OccuRate = 74.6875 (= 100*239/320)
STree: 350192384 bytes, OccuRate = 84.3026 (= 100*17365955/20599552)
BtmNodeM: 332233968 bytes = 324447 KiB = 316.843 MiB, OccuRate = 82.8237 (= 100*112958749/136384576)
BtmNodeS: 275021848 bytes = 268576 KiB = 262.281 MiB, OccuRate = 82.224 (= 100*112958977/137379616)
BtmNode: 607255816 bytes = 593023 KiB = 579.124 MiB, OccuRate = 82.5227 (= 100*225917726/273764192)
Links: 959225952 bytes = 936744 KiB = 914.789 MiB
Samples: 563642424 bytes = 550432 KiB = 537.531 MiB
---------------- additional details ----------------
Memory for weights: 251117272 bytes = 245232 KiB = 239.484 MiB
Over reserved: 2519376 bytes = 2460.33 KiB = 2.40266 MiB
DynRleForRlbwt object (0x7fff46fd5d80) printStatistics(0) END
OnlineLz77ViaRlbwt object (0x7fff46fd5d80) printStatistics(0) END
