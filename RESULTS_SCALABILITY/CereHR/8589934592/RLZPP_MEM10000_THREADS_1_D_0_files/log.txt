
--- computing reference lenght based on memory limit:
--- With 10000MB of  memory, the reference length will be: 1048575999 symbols
RLZ parser intitialized with:
Ignore     : 0
Ref        : 1048575999
Partitions : 3
Memory(MB) : 10000
RLZ parsing...
Building SA...
We will read blocks of size: 1835008001
Reading block 0...Block read in 9.46806 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1...Block read in 14.0862 (s)
Waiting for all tasks to be done...
Done.
Reading block 2...Block read in 14.0962 (s)
Starting parallel parsing in block...Parallel parse in 44.1036 (s)
Writing phrases... 
Phrases writed  in 0.018093 (s)
Waiting for all tasks to be done...
Done.
Reading block 3...Block read in 14.0086 (s)
Starting parallel parsing in block...Parallel parse in 93.1716 (s)
Writing phrases... 
Phrases writed  in 0.047012 (s)
Waiting for all tasks to be done...
Done.
Reading block 4...Block read in 14.0256 (s)
Starting parallel parsing in block...Parallel parse in 133.941 (s)
Writing phrases... 
Phrases writed  in 0.075477 (s)
Waiting for all tasks to be done...
Done.
Reading block 5...Block read in 1.57604 (s)
Starting parallel parsing in block...Parallel parse in 170.158 (s)
Writing phrases... 
Phrases writed  in 0.097891 (s)
Waiting for all tasks to be done...
Done.
Starting parallel parsing in block...Parallel parse in 21.9481 (s)
Writing phrases... 
Phrases writed  in 0.011307 (s)
Input succesfully compressed, output written in: CEREHR/8589934592/RLZPP_MEM10000_THREADS_1_D_0_files/compressed_file
Parsing performed in 1 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 1817998
Factors RLZ          : 12445108
Factors Total        : 14263106
--------------------------------------------
Time SA          : 121.317
Time LZ77 of ref : 33.4382
Time RLZ parse   : 521.376
-------------------------------
Time RLZ total   : 676.131


Final Wtime(s)    : 676.355
N Phrases         : 14263106
Compression ratio : 0.0101853
seconds per MiB   : 0.0825629
seconds per GiB   : 84.5444
MiB per second    : 12.112
GiB per second    : 0.0118281
