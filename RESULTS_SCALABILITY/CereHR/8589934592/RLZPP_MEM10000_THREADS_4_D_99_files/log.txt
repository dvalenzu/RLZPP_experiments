
--- computing reference lenght based on memory limit:
--- With 10000MB of  memory, the reference length will be: 1048575999 symbols
RLZ parser intitialized with:
Ignore     : 0
Ref        : 1048575999
Partitions : 12
Memory(MB) : 10000
RLZ parsing...
Building SA...
We will read blocks of size: 1835008001
Reading block 0...Block read in 8.07039 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1...Block read in 14.0242 (s)
Waiting for all tasks to be done...
Done.
Reading block 2...Block read in 13.5628 (s)
Starting parallel parsing in block...Parallel parse in 23.1964 (s)
Writing phrases... 
Phrases writed  in 0.01966 (s)
Waiting for all tasks to be done...
Done.
Reading block 3...Block read in 14.014 (s)
Starting parallel parsing in block...Parallel parse in 47.9993 (s)
Writing phrases... 
Phrases writed  in 0.050715 (s)
Waiting for all tasks to be done...
Done.
Reading block 4...Block read in 14.0244 (s)
Starting parallel parsing in block...Parallel parse in 69.4096 (s)
Writing phrases... 
Phrases writed  in 0.073556 (s)
Waiting for all tasks to be done...
Done.
Reading block 5...Block read in 1.51058 (s)
Starting parallel parsing in block...Parallel parse in 88.7485 (s)
Writing phrases... 
Phrases writed  in 0.102602 (s)
Waiting for all tasks to be done...
Done.
Starting parallel parsing in block...Parallel parse in 11.421 (s)
Writing phrases... 
Phrases writed  in 0.014996 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 10000MB of  memory, the reference length will be: 393215999 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 1817998
Ref        : 12445151
Partitions : 12
Memory(MB) : 10000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 14263149
Reading block 0...Block read in 0.031598 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 19
IM Random Access to sums file will use at most (MB): 32
 Random Access to Sum Files IM (delta compressed)CEREHR/8589934592/RLZPP_MEM10000_THREADS_4_D_99_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: CEREHR/8589934592/RLZPP_MEM10000_THREADS_4_D_99_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 1817998
Factors RLZ          : 12445151
Factors Total        : 14263149
--------------------------------------------
Time SA          : 119.482
Time LZ77 of ref : 33.2871
Time RLZ parse   : 298.185
-------------------------------
Time RLZ total   : 450.954
--------------------------------------
Rlz TOTAL time 	: 451.182
Pack      time 	: 0.502298
Recursion time 	: 10.5087
ReParse   time 	: 0.951416
Encode    time 	: 0
--------------------------------------
TOTAL     time 	: 463.145

1-th level report: 

Factors skipped      : 1817998
Factors 77'ed in ref : 2651486
Factors RLZ          : 0
Factors Total        : 4469484
--------------------------------------------
Time SA          : 9.79778
Time LZ77 of ref : 0.659961
Time RLZ parse   : 1.00001e-06
-------------------------------
Time RLZ total   : 10.4577


Final Wtime(s)    : 463.145
N Phrases         : 4469484
Compression ratio : 0.00308506
seconds per MiB   : 0.0565362
seconds per GiB   : 57.8931
MiB per second    : 17.6878
GiB per second    : 0.0172732
