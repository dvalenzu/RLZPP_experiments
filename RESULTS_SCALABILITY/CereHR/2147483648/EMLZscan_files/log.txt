Text filename = /home/work/dvalenzu/data/cereHR.2GiB
Output filename = /home/local/dvalenzu/Repos/Code/RLZPP_experiments/CEREHR/2147483648/EMLZscan_files/compressed.lz
RAM use = 10485760000 (10000.00MiB)
Max block size = 299593142 (285.71MiB)
sizeof(textoff_t) = 6
sizeof(blockoff_t) = 5

Process block [0..299593142):
  Read block: 0.09s
  Compute SA of block: 27.80s
  Parse block: 5.52s
  Current progress: 0.0%, elapsed = 34s, z = 1578586, total I/O vol = 0.15n
Process block [299553730..599146872):
  Read block: 0.09s
  Compute SA of block: 27.61s
  Compute LCP of block: 10.36s
  Compute MS-support: 5.05s
  Stream:   Stream: 100.0%, time = 50.41, I/O = 5.67MB/s
  Invert matching statistics: 2.87s
  Parse block: 4.84s
  Current progress: 13.9%, elapsed = 136s, z = 1729795, total I/O vol = 0.43n
Process block [599144528..898737670):
  Read block: 0.08s
  Compute SA of block: 27.88s
  Compute LCP of block: 10.33s
  Compute MS-support: 5.09s
  Stream:   Stream: 100.0%, time = 76.83, I/O = 7.44MB/s
  Invert matching statistics: 2.17s
  Parse block: 4.75s
  Current progress: 27.9%, elapsed = 264s, z = 1787661, total I/O vol = 0.85n
Process block [898723923..1198317065):
  Read block: 0.09s
  Compute SA of block: 27.75s
  Compute LCP of block: 10.36s
  Compute MS-support: 5.11s
  Stream:   Stream: 100.0%, time = 99.91, I/O = 8.58MB/s
  Invert matching statistics: 1.68s
  Parse block: 4.72s
  Current progress: 41.9%, elapsed = 414s, z = 1850031, total I/O vol = 1.41n
Process block [1198308011..1497901153):
  Read block: 0.09s
  Compute SA of block: 27.53s
  Compute LCP of block: 10.54s
  Compute MS-support: 5.09s
  Stream:   Stream: 100.0%, time = 112.98, I/O = 10.11MB/s
  Invert matching statistics: 1.52s
  Parse block: 4.77s
  Current progress: 55.8%, elapsed = 578s, z = 1906685, total I/O vol = 2.11n
Process block [1497889892..1797483034):
  Read block: 0.09s
  Compute SA of block: 27.74s
  Compute LCP of block: 10.46s
  Compute MS-support: 5.09s
  Stream:   Stream: 100.0%, time = 130.09, I/O = 10.98MB/s
  Invert matching statistics: 1.56s
  Parse block: 5.04s
  Current progress: 69.8%, elapsed = 759s, z = 1965681, total I/O vol = 2.95n
Process block [1797478239..2097071381):
  Read block: 0.09s
  Compute SA of block: 28.32s
  Compute LCP of block: 10.70s
  Compute MS-support: 5.10s
  Stream:   Stream: 100.0%, time = 135.71, I/O = 12.63MB/s
  Invert matching statistics: 1.40s
  Parse block: 4.74s
  Current progress: 83.7%, elapsed = 945s, z = 2027144, total I/O vol = 3.93n
Process block [2097066352..2147483648):
  Read block: 0.01s
  Compute SA of block: 4.05s
  Compute LCP of block: 1.61s
  Compute MS-support: 0.77s
  Stream:   Stream: 100.0%, time = 48.24, I/O = 41.45MB/s
  Invert matching statistics: 0.21s
  Parse block: 0.75s
  Current progress: 97.7%, elapsed = 1001s, z = 2037942, total I/O vol = 4.93n


Computation finished. Summary:
  elapsed time = 1001.31s (0.489s/MiB of text)
  speed = 2.05MiB of text/s
  I/O volume = 10582878114bytes (4.93bytes/input symbol)
  Number of phrases = 2037942
  Average phrase length = 1053.751
Final Wtime(s) : 1001.31
N Phrases      : 2037942
	Command being timed: "./ext/EM-LZscan-0.2/src/emlz_parser -o CEREHR/2147483648/EMLZscan_files/compressed.lz -m10000 /home/work/dvalenzu/data/cereHR.2GiB"
	User time (seconds): 982.81
	System time (seconds): 18.48
	Percent of CPU this job got: 99%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 16:41.30
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 8535640
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 18869884
	Voluntary context switches: 39
	Involuntary context switches: 1072
	Swaps: 0
	File system inputs: 0
	File system outputs: 76984
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
