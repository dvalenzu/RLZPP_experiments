Text filename = /home/work/dvalenzu/data/cereHR.1GiB
Output filename = /home/local/dvalenzu/Repos/Code/RLZPP_experiments/CEREHR/1073741824/EMLZscan_files/compressed.lz
RAM use = 10485760000 (10000.00MiB)
Max block size = 299593142 (285.71MiB)
sizeof(textoff_t) = 6
sizeof(blockoff_t) = 5

Process block [0..299593142):
  Read block: 0.09s
  Compute SA of block: 27.89s
  Parse block: 5.45s
  Current progress: 0.0%, elapsed = 34s, z = 1578586, total I/O vol = 0.30n
Process block [299553730..599146872):
  Read block: 0.09s
  Compute SA of block: 27.74s
  Compute LCP of block: 10.35s
  Compute MS-support: 5.11s
  Stream:   Stream: 100.0%, time = 50.59, I/O = 5.65MB/s
  Invert matching statistics: 2.86s
  Parse block: 4.75s
  Current progress: 27.9%, elapsed = 136s, z = 1729795, total I/O vol = 0.87n
Process block [599144528..898737670):
  Read block: 0.08s
  Compute SA of block: 27.84s
  Compute LCP of block: 10.33s
  Compute MS-support: 5.06s
  Stream:   Stream: 100.0%, time = 77.03, I/O = 7.42MB/s
  Invert matching statistics: 2.17s
  Parse block: 4.71s
  Current progress: 55.8%, elapsed = 264s, z = 1787661, total I/O vol = 1.70n
Process block [898723923..1073741824):
  Read block: 0.05s
  Compute SA of block: 15.43s
  Compute LCP of block: 5.19s
  Compute MS-support: 2.89s
  Stream:   Stream: 100.0%, time = 73.37, I/O = 11.68MB/s
  Invert matching statistics: 0.85s
  Parse block: 2.69s
  Current progress: 83.7%, elapsed = 365s, z = 1823225, total I/O vol = 2.71n


Computation finished. Summary:
  elapsed time = 365.32s (0.357s/MiB of text)
  speed = 2.80MiB of text/s
  I/O volume = 2905944941bytes (2.71bytes/input symbol)
  Number of phrases = 1823225
  Average phrase length = 588.924
Final Wtime(s) : 365.321
N Phrases      : 1823225
	Command being timed: "./ext/EM-LZscan-0.2/src/emlz_parser -o CEREHR/1073741824/EMLZscan_files/compressed.lz -m10000 /home/work/dvalenzu/data/cereHR.1GiB"
	User time (seconds): 357.39
	System time (seconds): 7.92
	Percent of CPU this job got: 100%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 6:05.32
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 8535804
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 8811495
	Voluntary context switches: 26
	Involuntary context switches: 322
	Swaps: 0
	File system inputs: 0
	File system outputs: 67448
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
