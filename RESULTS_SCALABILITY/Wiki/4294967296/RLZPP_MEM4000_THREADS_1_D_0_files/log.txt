
--- computing reference lenght based on memory limit:
--- With 4000MB of  memory, the reference length will be: 419430399 symbols
RLZ parser intitialized with:
Ignore     : 0
Ref        : 419430399
Partitions : 3
Memory(MB) : 4000
RLZ parsing...
Building SA...
We will read blocks of size: 734003201
Reading block 0/6
Block read in 0.142767 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/6
Block read in 0.229078 (s)
Waiting for all tasks to be done...
Done.
Reading block 2/6
Block read in 0.225231 (s)

*
Starting parallel parsing in block...Parallel parse in 187.551 (s)
Writing phrases... 
Phrases written  in 0.331997 (s)
Waiting for all tasks to be done...
Done.
Reading block 3/6
Block read in 0.122168 (s)

*
Starting parallel parsing in block...Parallel parse in 218.671 (s)
Writing phrases... 
Phrases written  in 0.379295 (s)
Waiting for all tasks to be done...
Done.
Reading block 4/6
Block read in 0.122542 (s)

*
Starting parallel parsing in block...Parallel parse in 185.6 (s)
Writing phrases... 
Phrases written  in 0.33968 (s)
Waiting for all tasks to be done...
Done.
Reading block 5/6
Block read in 0.122353 (s)

*
Starting parallel parsing in block...Parallel parse in 204.097 (s)
Writing phrases... 
Phrases written  in 0.355202 (s)
Waiting for all tasks to be done...
Done.
Reading block 6/6
Block read in 0.034638 (s)

*
Starting parallel parsing in block...Parallel parse in 182.615 (s)
Writing phrases... 
Phrases written  in 0.316344 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 52.2546 (s)
Writing phrases... 
Phrases written  in 0.091864 (s)
Input succesfully compressed, output written in: RESULTS_SCALABILITY/WIKIPEDIA//4294967296/RLZPP_MEM4000_THREADS_1_D_0_files/compressed_file
Parsing performed in 1 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 7088095
Factors RLZ          : 90379738
Factors Total        : 97467833
--------------------------------------------
Time SA          : 36.249
Time LZ77 of ref : 10.7964
Time RLZ parse   : 1033.49
-------------------------------
Time RLZ total   : 1080.54


Final Wtime(s)    : 1080.64
N Phrases         : 97467833
Compression ratio : 0.121902
seconds per MiB   : 0.263828
seconds per GiB   : 270.16
MiB per second    : 3.79034
GiB per second    : 0.00370151
Input filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/WIKIPEDIA/4294967296/RLZPP_MEM4000_THREADS_1_D_0_files/compressed_file
Output filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/WIKIPEDIA/4294967296/RLZPP_MEM4000_THREADS_1_D_0_files/compressed_file.decompressed
Input size = 523565020 (499.3MiB)
Max disk use = 1152921504606846976 (1099511627776.0MiB)
RAM use = 3758096384 (3584.0MiB)
RAM use (excl. buffer) = 3724541952 (3552.0MiB)
Max segment size = 3724541952 (3552.0MiB)

Process part 1:
  Total parsing data decoded so far: 0.00%
  Total text decoded so far: 0 (0.00MiB)
  Permute phrases by source:   Permute phrases by source: time = 0.38s, I/O = 28.32MiB/s  Permute phrases by source: time = 0.43s, I/O = 50.57MiB/s  Permute phrases by source: time = 0.47s, I/O = 70.56MiB/s  Permute phrases by source: time = 0.53s, I/O = 83.42MiB/s  Permute phrases by source: time = 0.59s, I/O = 94.41MiB/s  Permute phrases by source: time = 0.62s, I/O = 107.71MiB/s  Permute phrases by source: time = 0.71s, I/O = 112.37MiB/s  Permute phrases by source: time = 0.77s, I/O = 118.90MiB/s  Permute phrases by source: time = 0.80s, I/O = 127.95MiB/s  Permute phrases by source: time = 0.87s, I/O = 131.37MiB/s  Permute phrases by source: time = 0.94s, I/O = 134.98MiB/s  Permute phrases by source: time = 0.97s, I/O = 141.83MiB/s  Permute phrases by source: time = 1.04s, I/O = 143.74MiB/s  Permute phrases by source: time = 1.11s, I/O = 146.06MiB/s  Permute phrases by source: time = 1.14s, I/O = 151.52MiB/s  Permute phrases by source: time = 1.21s, I/O = 152.64MiB/s  Permute phrases by source: time = 1.28s, I/O = 154.17MiB/s  Permute phrases by source: time = 1.32s, I/O = 158.75MiB/s  Permute phrases by source: time = 1.38s, I/O = 159.36MiB/s  Permute phrases by source: time = 1.45s, I/O = 160.56MiB/s  Permute phrases by source: time = 1.49s, I/O = 164.40MiB/s  Permute phrases by source: time = 1.55s, I/O = 164.71MiB/s  Permute phrases by source: time = 1.62s, I/O = 165.45MiB/s  Permute phrases by source: time = 1.66s, I/O = 168.78MiB/s  Permute phrases by source: time = 1.72s, I/O = 168.88MiB/s  Permute phrases by source: time = 1.79s, I/O = 169.41MiB/s  Permute phrases by source: time = 1.83s, I/O = 172.35MiB/s  Permute phrases by source: time = 1.89s, I/O = 172.36MiB/s  Permute phrases by source: time = 1.95s, I/O = 173.06MiB/s  Permute phrases by source: time = 2.02s, I/O = 173.25MiB/s  Permute phrases by source: time = 2.06s, I/O = 175.80MiB/s  Permute phrases by source: time = 2.12s, I/O = 176.35MiB/s  Permute phrases by source: time = 2.18s, I/O = 176.41MiB/s  Permute phrases by source: time = 2.22s, I/O = 178.69MiB/s  Permute phrases by source: time = 2.28s, I/O = 179.14MiB/s  Permute phrases by source: time = 2.35s, I/O = 179.14MiB/s  Permute phrases by source: time = 2.39s, I/O = 181.23MiB/s  Permute phrases by source: time = 2.45s, I/O = 181.50MiB/s  Permute phrases by source: time = 2.51s, I/O = 181.51MiB/s  Permute phrases by source: time = 2.55s, I/O = 183.44MiB/s  Permute phrases by source: time = 2.61s, I/O = 183.60MiB/s  Permute phrases by source: time = 2.68s, I/O = 183.59MiB/s  Permute phrases by source: time = 2.71s, I/O = 185.35MiB/s  Permute phrases by source: time = 2.78s, I/O = 185.51MiB/s  Permute phrases by source: time = 2.84s, I/O = 185.35MiB/s  Permute phrases by source: time = 2.88s, I/O = 187.00MiB/s  Permute phrases by source: time = 2.95s, I/O = 186.64MiB/s  Permute phrases by source: time = 3.01s, I/O = 186.64MiB/s  Permute phrases by source: time = 3.05s, I/O = 188.24MiB/s  Permute phrases by source: time = 3.11s, I/O = 188.48MiB/s  Permute phrases by source: time = 3.17s, I/O = 188.37MiB/s  Permute phrases by source: time = 3.21s, I/O = 189.81MiB/s  Permute phrases by source: time = 3.27s, I/O = 190.03MiB/s  Permute phrases by source: time = 3.33s, I/O = 189.91MiB/s  Permute phrases by source: time = 3.37s, I/O = 191.27MiB/s  Permute phrases by source: time = 3.43s, I/O = 191.43MiB/s  Permute phrases by source: time = 3.49s, I/O = 191.36MiB/s  Permute phrases by source: time = 3.53s, I/O = 192.64MiB/s  Permute phrases by source: time = 3.59s, I/O = 192.78MiB/s  Permute phrases by source: time = 3.65s, I/O = 192.61MiB/s  Permute phrases by source: time = 3.69s, I/O = 193.82MiB/s  Permute phrases by source: time = 3.75s, I/O = 193.93MiB/s  Permute phrases by source: time = 3.81s, I/O = 193.77MiB/s  Permute phrases by source: time = 3.85s, I/O = 194.92MiB/s  Permute phrases by source: time = 3.91s, I/O = 195.03MiB/s  Permute phrases by source: time = 3.99s, I/O = 194.24MiB/s  Permute phrases by source: time = 4.03s, I/O = 195.32MiB/s  Permute phrases by source: time = 4.09s, I/O = 195.20MiB/s  Permute phrases by source: time = 4.16s, I/O = 194.81MiB/s  Permute phrases by source: time = 4.19s, I/O = 195.85MiB/s  Permute phrases by source: time = 4.26s, I/O = 195.73MiB/s  Permute phrases by source: time = 4.33s, I/O = 195.23MiB/s  Permute phrases by source: time = 4.37s, I/O = 196.22MiB/s  Permute phrases by source: time = 4.43s, I/O = 196.15MiB/s  Permute phrases by source: time = 4.50s, I/O = 195.76MiB/s  Permute phrases by source: time = 4.56s, I/O = 195.60MiB/s  Permute phrases by source: time = 4.60s, I/O = 196.55MiB/s  Permute phrases by source: time = 4.67s, I/O = 196.18MiB/s  Permute phrases by source: time = 4.73s, I/O = 196.00MiB/s  Permute phrases by source: time = 4.77s, I/O = 196.92MiB/s  Permute phrases by source: time = 4.86s, I/O = 195.55MiB/s  Permute phrases by source: time = 4.94s, I/O = 194.72MiB/s  Permute phrases by source: time = 4.99s, I/O = 195.19MiB/s  Permute phrases by source: time = 5.06s, I/O = 194.95MiB/s  Permute phrases by source: time = 5.12s, I/O = 194.75MiB/s  Permute phrases by source: time = 5.16s, I/O = 195.58MiB/s  Permute phrases by source: time = 5.23s, I/O = 195.43MiB/s  Permute phrases by source: time = 5.29s, I/O = 195.39MiB/s  Permute phrases by source: time = 5.33s, I/O = 196.20MiB/s  Permute phrases by source: time = 5.36s, I/O = 197.01MiB/s  Permute phrases by source: time = 5.40s, I/O = 197.80MiB/s  Permute phrases by source: time = 5.44s, I/O = 198.59MiB/s  Permute phrases by source: 100.0%, time = 5.48s, I/O = 199.33MiB/s
  Processing this part will decode 4096.0MiB of text (100.00% of parsing)
  Last part = TRUE
  Decode segment 1/2 [0..3724541952):
    Selfdecode segment:     Selfdecode segment: time = 0.07s, I/O = 86.10MiB/s    Selfdecode segment: time = 0.15s, I/O = 78.39MiB/s    Selfdecode segment: time = 0.24s, I/O = 74.28MiB/s    Selfdecode segment: time = 0.34s, I/O = 71.20MiB/s    Selfdecode segment: time = 0.43s, I/O = 69.51MiB/s    Selfdecode segment: time = 0.54s, I/O = 68.28MiB/s    Selfdecode segment: time = 0.64s, I/O = 67.46MiB/s    Selfdecode segment: time = 0.73s, I/O = 67.70MiB/s    Selfdecode segment: time = 0.82s, I/O = 68.17MiB/s    Selfdecode segment: time = 0.91s, I/O = 68.32MiB/s    Selfdecode segment: time = 1.00s, I/O = 68.57MiB/s    Selfdecode segment: time = 1.08s, I/O = 69.50MiB/s    Selfdecode segment: time = 1.16s, I/O = 70.05MiB/s    Selfdecode segment: time = 1.25s, I/O = 70.35MiB/s    Selfdecode segment: time = 1.33s, I/O = 70.60MiB/s    Selfdecode segment: time = 1.42s, I/O = 70.86MiB/s    Selfdecode segment: time = 1.51s, I/O = 70.77MiB/s    Selfdecode segment: time = 1.60s, I/O = 70.95MiB/s    Selfdecode segment: time = 1.68s, I/O = 71.16MiB/s    Selfdecode segment: time = 1.77s, I/O = 71.42MiB/s    Selfdecode segment: time = 1.85s, I/O = 71.73MiB/s    Selfdecode segment: time = 1.93s, I/O = 71.88MiB/s    Selfdecode segment: time = 2.02s, I/O = 72.03MiB/s    Selfdecode segment: time = 2.10s, I/O = 72.12MiB/s    Selfdecode segment: time = 2.20s, I/O = 71.97MiB/s    Selfdecode segment: time = 2.28s, I/O = 72.05MiB/s    Selfdecode segment: time = 2.37s, I/O = 72.13MiB/s    Selfdecode segment: time = 2.45s, I/O = 72.25MiB/s    Selfdecode segment: time = 2.54s, I/O = 72.34MiB/s    Selfdecode segment: time = 2.62s, I/O = 72.43MiB/s    Selfdecode segment: time = 2.72s, I/O = 72.33MiB/s    Selfdecode segment: time = 2.80s, I/O = 72.30MiB/s    Selfdecode segment: time = 2.89s, I/O = 72.30MiB/s    Selfdecode segment: time = 2.97s, I/O = 72.47MiB/s    Selfdecode segment: time = 3.06s, I/O = 72.51MiB/s    Selfdecode segment: time = 3.14s, I/O = 72.78MiB/s    Selfdecode segment: time = 3.22s, I/O = 73.01MiB/s    Selfdecode segment: time = 3.29s, I/O = 73.35MiB/s    Selfdecode segment: time = 3.37s, I/O = 73.40MiB/s    Selfdecode segment: time = 3.46s, I/O = 73.44MiB/s    Selfdecode segment: time = 3.54s, I/O = 73.43MiB/s    Selfdecode segment: time = 3.63s, I/O = 73.38MiB/s    Selfdecode segment: time = 3.72s, I/O = 73.41MiB/s    Selfdecode segment: time = 3.80s, I/O = 73.46MiB/s    Selfdecode segment: time = 3.89s, I/O = 73.53MiB/s    Selfdecode segment: time = 3.97s, I/O = 73.63MiB/s    Selfdecode segment: time = 4.06s, I/O = 73.56MiB/s    Selfdecode segment: time = 4.15s, I/O = 73.51MiB/s    Selfdecode segment: time = 4.18s, I/O = 74.47MiB/s    Selfdecode segment: time = 4.24s, I/O = 75.07MiB/s    Selfdecode segment: time = 4.32s, I/O = 75.05MiB/s    Selfdecode segment: time = 4.41s, I/O = 75.01MiB/s    Selfdecode segment: time = 4.49s, I/O = 75.02MiB/s    Selfdecode segment: time = 4.58s, I/O = 75.07MiB/s    Selfdecode segment: time = 4.65s, I/O = 75.18MiB/s    Selfdecode segment: time = 4.75s, I/O = 75.07MiB/s    Selfdecode segment: time = 4.83s, I/O = 75.16MiB/s    Selfdecode segment: time = 4.91s, I/O = 75.16MiB/s    Selfdecode segment: time = 4.99s, I/O = 75.19MiB/s    Selfdecode segment: time = 5.08s, I/O = 75.19MiB/s    Selfdecode segment: time = 5.17s, I/O = 75.15MiB/s    Selfdecode segment: time = 5.25s, I/O = 75.10MiB/s    Selfdecode segment: time = 5.34s, I/O = 75.06MiB/s    Selfdecode segment: time = 5.43s, I/O = 75.01MiB/s    Selfdecode segment: time = 5.53s, I/O = 74.85MiB/s    Selfdecode segment: time = 5.63s, I/O = 74.69MiB/s    Selfdecode segment: time = 5.70s, I/O = 74.80MiB/s    Selfdecode segment: time = 5.78s, I/O = 74.91MiB/s    Selfdecode segment: time = 5.87s, I/O = 74.82MiB/s    Selfdecode segment: time = 5.96s, I/O = 74.82MiB/s    Selfdecode segment: time = 6.05s, I/O = 74.79MiB/s    Selfdecode segment: time = 6.13s, I/O = 74.75MiB/s    Selfdecode segment: time = 6.21s, I/O = 74.82MiB/s    Selfdecode segment: time = 6.30s, I/O = 74.80MiB/s    Selfdecode segment: time = 6.40s, I/O = 74.63MiB/s    Selfdecode segment: time = 6.49s, I/O = 74.61MiB/s    Selfdecode segment: time = 6.58s, I/O = 74.56MiB/s    Selfdecode segment: time = 6.67s, I/O = 74.53MiB/s    Selfdecode segment: time = 6.75s, I/O = 74.51MiB/s    Selfdecode segment: time = 6.85s, I/O = 74.44MiB/s    Selfdecode segment: time = 6.92s, I/O = 74.40MiB/s
    Distribute chunks:     Distribute chunks: time = 0.02s, I/O = 422.40MiB/s    Distribute chunks: time = 0.05s, I/O = 468.80MiB/s    Distribute chunks: time = 0.09s, I/O = 495.09MiB/s    Distribute chunks: time = 0.14s, I/O = 517.07MiB/s    Distribute chunks: time = 0.19s, I/O = 526.59MiB/s    Distribute chunks: time = 0.26s, I/O = 518.34MiB/s    Distribute chunks: time = 0.33s, I/O = 502.94MiB/s    Distribute chunks: time = 0.41s, I/O = 537.28MiB/s    Distribute chunks: time = 0.51s, I/O = 562.07MiB/s    Distribute chunks: time = 0.60s, I/O = 565.05MiB/s    Distribute chunks: time = 0.69s, I/O = 556.67MiB/s    Distribute chunks: time = 0.79s, I/O = 563.16MiB/s    Distribute chunks: time = 0.80s, I/O = 563.82MiB/s
    Write segment to disk: 26.38s, I/O = 134.66MiB/s
    Decode the nearby phrases for next segment: 0.96s
  Decode segment 2/2 [3724541952..4294967296):
    Load segment chunks:     Load segment chunks: 1.6%, time = 0.09s, I/O = 68.58MiB/s    Load segment chunks: 2.7%, time = 0.13s, I/O = 76.59MiB/s    Load segment chunks: 3.7%, time = 0.17s, I/O = 80.91MiB/s    Load segment chunks: 4.8%, time = 0.21s, I/O = 84.25MiB/s    Load segment chunks: 5.9%, time = 0.26s, I/O = 85.97MiB/s    Load segment chunks: 7.5%, time = 0.31s, I/O = 89.97MiB/s    Load segment chunks: 8.5%, time = 0.35s, I/O = 90.56MiB/s    Load segment chunks: 9.6%, time = 0.39s, I/O = 91.32MiB/s    Load segment chunks: 10.7%, time = 0.44s, I/O = 91.91MiB/s    Load segment chunks: 12.3%, time = 0.49s, I/O = 94.16MiB/s    Load segment chunks: 13.3%, time = 0.54s, I/O = 93.18MiB/s    Load segment chunks: 14.4%, time = 0.58s, I/O = 93.39MiB/s    Load segment chunks: 15.5%, time = 0.62s, I/O = 93.70MiB/s    Load segment chunks: 16.5%, time = 0.66s, I/O = 93.88MiB/s    Load segment chunks: 18.1%, time = 0.72s, I/O = 95.09MiB/s    Load segment chunks: 19.2%, time = 0.76s, I/O = 95.19MiB/s    Load segment chunks: 20.3%, time = 0.80s, I/O = 95.33MiB/s    Load segment chunks: 21.3%, time = 0.84s, I/O = 95.42MiB/s    Load segment chunks: 22.9%, time = 0.89s, I/O = 96.23MiB/s    Load segment chunks: 24.5%, time = 1.02s, I/O = 89.98MiB/s    Load segment chunks: 25.6%, time = 1.04s, I/O = 91.89MiB/s    Load segment chunks: 26.7%, time = 1.16s, I/O = 86.03MiB/s    Load segment chunks: 27.7%, time = 1.21s, I/O = 86.12MiB/s    Load segment chunks: 29.3%, time = 1.27s, I/O = 86.88MiB/s    Load segment chunks: 30.4%, time = 1.31s, I/O = 86.81MiB/s    Load segment chunks: 32.0%, time = 1.37s, I/O = 87.46MiB/s    Load segment chunks: 33.1%, time = 1.42s, I/O = 87.49MiB/s    Load segment chunks: 34.1%, time = 1.46s, I/O = 87.54MiB/s    Load segment chunks: 35.7%, time = 1.52s, I/O = 88.12MiB/s    Load segment chunks: 36.8%, time = 1.57s, I/O = 88.14MiB/s    Load segment chunks: 37.9%, time = 1.61s, I/O = 88.29MiB/s    Load segment chunks: 38.9%, time = 1.72s, I/O = 84.71MiB/s    Load segment chunks: 40.0%, time = 1.77s, I/O = 84.92MiB/s    Load segment chunks: 41.6%, time = 1.82s, I/O = 85.67MiB/s    Load segment chunks: 42.7%, time = 1.86s, I/O = 85.88MiB/s    Load segment chunks: 43.7%, time = 1.91s, I/O = 86.05MiB/s    Load segment chunks: 44.8%, time = 1.95s, I/O = 86.29MiB/s    Load segment chunks: 46.4%, time = 2.00s, I/O = 86.85MiB/s    Load segment chunks: 47.5%, time = 2.05s, I/O = 86.98MiB/s    Load segment chunks: 48.5%, time = 2.09s, I/O = 87.12MiB/s    Load segment chunks: 49.6%, time = 2.13s, I/O = 87.34MiB/s    Load segment chunks: 50.6%, time = 2.17s, I/O = 87.50MiB/s    Load segment chunks: 51.7%, time = 2.22s, I/O = 87.52MiB/s    Load segment chunks: 53.3%, time = 2.27s, I/O = 88.02MiB/s    Load segment chunks: 54.4%, time = 2.31s, I/O = 88.17MiB/s    Load segment chunks: 55.4%, time = 2.36s, I/O = 88.31MiB/s    Load segment chunks: 56.5%, time = 2.40s, I/O = 88.39MiB/s    Load segment chunks: 58.1%, time = 2.45s, I/O = 88.95MiB/s    Load segment chunks: 59.2%, time = 2.49s, I/O = 89.03MiB/s    Load segment chunks: 60.2%, time = 2.62s, I/O = 86.41MiB/s    Load segment chunks: 61.8%, time = 2.64s, I/O = 87.99MiB/s    Load segment chunks: 62.9%, time = 2.74s, I/O = 86.15MiB/s    Load segment chunks: 64.0%, time = 2.78s, I/O = 86.48MiB/s    Load segment chunks: 65.0%, time = 2.84s, I/O = 86.04MiB/s    Load segment chunks: 66.6%, time = 2.89s, I/O = 86.38MiB/s    Load segment chunks: 67.7%, time = 2.94s, I/O = 86.44MiB/s    Load segment chunks: 68.8%, time = 2.98s, I/O = 86.48MiB/s    Load segment chunks: 69.8%, time = 3.03s, I/O = 86.54MiB/s    Load segment chunks: 71.4%, time = 3.09s, I/O = 86.84MiB/s    Load segment chunks: 72.5%, time = 3.13s, I/O = 86.89MiB/s    Load segment chunks: 73.6%, time = 3.18s, I/O = 86.89MiB/s    Load segment chunks: 75.2%, time = 3.23s, I/O = 87.21MiB/s    Load segment chunks: 76.2%, time = 3.28s, I/O = 87.25MiB/s    Load segment chunks: 77.3%, time = 3.33s, I/O = 87.17MiB/s    Load segment chunks: 78.9%, time = 3.38s, I/O = 87.53MiB/s    Load segment chunks: 80.0%, time = 3.42s, I/O = 87.60MiB/s    Load segment chunks: 81.0%, time = 3.47s, I/O = 87.65MiB/s    Load segment chunks: 82.6%, time = 3.52s, I/O = 87.96MiB/s    Load segment chunks: 84.2%, time = 3.64s, I/O = 86.73MiB/s    Load segment chunks: 85.3%, time = 3.73s, I/O = 85.88MiB/s    Load segment chunks: 86.4%, time = 3.77s, I/O = 85.95MiB/s    Load segment chunks: 87.4%, time = 3.81s, I/O = 86.07MiB/s    Load segment chunks: 88.5%, time = 3.86s, I/O = 86.10MiB/s    Load segment chunks: 90.1%, time = 3.92s, I/O = 86.33MiB/s    Load segment chunks: 91.2%, time = 3.96s, I/O = 86.42MiB/s    Load segment chunks: 92.2%, time = 4.00s, I/O = 86.48MiB/s    Load segment chunks: 93.3%, time = 4.04s, I/O = 86.57MiB/s    Load segment chunks: 94.9%, time = 4.10s, I/O = 86.85MiB/s    Load segment chunks: 96.0%, time = 4.14s, I/O = 86.94MiB/s    Load segment chunks: 97.0%, time = 4.18s, I/O = 87.01MiB/s    Load segment chunks: 98.1%, time = 4.22s, I/O = 87.11MiB/s    Load segment chunks: 99.7%, time = 4.28s, I/O = 87.42MiB/s    Load segment chunks: 100.0%, time = 4.29s, I/O = 87.46MiB/s


Computation finished. Summary:
  I/O volume = 6276889978 (1.46B/B)
  number of decoded phrases: 97467833
  average phrase length = 44.07
  length of decoded text: 4294967296 (4096.00MiB)
  elapsed time: 45.46s (0.0111s/MiB)
  speed: 90.10MiB/s
Decompression t(s)    : 45.4997310638
