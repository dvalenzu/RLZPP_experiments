Text filename = /home/work/dvalenzu/data/large/Wiki.512MiB
Output filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/WIKIPEDIA/536870912/EMLZscan_files/compressed.lz
RAM use = 4194304000 (4000.00MiB)
Max block size = 119837257 (114.29MiB)
sizeof(textoff_t) = 6
sizeof(blockoff_t) = 5

Process block [0..119837257):
  Read block: 0.04s
  Compute SA of block: 9.00s
  Parse block: 2.47s
  Current progress: 0.0%, elapsed = 12s, z = 2041854, total I/O vol = 0.28n
Process block [119836768..239674025):
  Read block: 0.04s
  Compute SA of block: 9.06s
  Compute LCP of block: 3.73s
  Compute MS-support: 2.33s
  Stream:   Stream: 100.0%, time = 16.50, I/O = 6.93MB/s
  Invert matching statistics: 0.84s
  Parse block: 2.57s
  Current progress: 22.3%, elapsed = 47s, z = 4294166, total I/O vol = 0.80n
Process block [239669898..359507155):
  Read block: 0.02s
  Compute SA of block: 8.95s
  Compute LCP of block: 3.71s
  Compute MS-support: 2.27s
  Stream:   Stream: 100.0%, time = 33.27, I/O = 6.87MB/s
  Invert matching statistics: 0.88s
  Parse block: 2.52s
  Current progress: 44.6%, elapsed = 99s, z = 6329334, total I/O vol = 1.54n
Process block [359507138..479344395):
  Read block: 0.02s
  Compute SA of block: 8.96s
  Compute LCP of block: 3.65s
  Compute MS-support: 2.25s
  Stream:   Stream: 100.0%, time = 49.08, I/O = 6.99MB/s
  Invert matching statistics: 0.83s
  Parse block: 2.41s
  Current progress: 67.0%, elapsed = 167s, z = 7955320, total I/O vol = 2.50n
Process block [479342902..536870912):
  Read block: 0.01s
  Compute SA of block: 3.93s
  Compute LCP of block: 1.74s
  Compute MS-support: 1.02s
  Stream:   Stream: 100.0%, time = 54.68, I/O = 8.36MB/s
  Invert matching statistics: 0.35s
  Parse block: 1.16s
  Current progress: 89.3%, elapsed = 230s, z = 8817307, total I/O vol = 3.53n


Computation finished. Summary:
  elapsed time = 229.69s (0.449s/MiB of text)
  speed = 2.23MiB of text/s
  I/O volume = 1896966356bytes (3.53bytes/input symbol)
  Number of phrases = 8817307
  Average phrase length = 60.888
Final Wtime(s) : 229.692
N Phrases      : 8817307
	Command being timed: "./ext/EM-LZscan-0.2/src/emlz_parser -o RESULTS_SCALABILITY/WIKIPEDIA//536870912/EMLZscan_files/compressed.lz -m4000 /home/work/dvalenzu/data/large/Wiki.512MiB"
	User time (seconds): 225.42
	System time (seconds): 4.32
	Percent of CPU this job got: 100%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 3:49.70
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 3984912
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 1
	Minor (reclaiming a frame) page faults: 4565836
	Voluntary context switches: 87
	Involuntary context switches: 366
	Swaps: 0
	File system inputs: 328
	File system outputs: 310320
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
