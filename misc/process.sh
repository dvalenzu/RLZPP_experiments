## This assumes it went until level 2, and collect for thread 1, 8 , 16, 64
touch data.txt titles.txt;
rm data.txt titles.txt;
EXP_FOLDER="../WIKI_EXP2/"
for THREADS in 1 8 16 64
do
  #echo ${THREADS}
  CURR_FOLDER="${EXP_FOLDER}/RLZ_PLUS_THREADS_${THREADS}_DEPTH_99_MEM_10000_files/"
  LOG_FILE="${CURR_FOLDER}/log.txt"
  printf ${THREADS} >> data.txt
  cat ${LOG_FILE} | grep -v "e-06\|TOTAL\|Recursion time"   | grep "ummary\|time\|Time"  | head -n 16  | grep -v "um" | cut -d: -f2  | tac | tr "\n" " " >> data.txt
  cat ${LOG_FILE} | grep -v "e-06\|TOTAL\|Recursion time"   | grep "ummary\|time\|Time"  | head -n 16                 | cut -d: -f1  | tac | tr "\n" " " | tr -d "\t" | sed 's/  */ /g' >> titles.txt
  echo "" >> data.txt
  echo "" >> titles.txt
done
