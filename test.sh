#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail 

FILE="ext/ReLZ/tests/tests_data/einstein.en.sample.txt"
DIR="TMP_UTESTRESULTS_EINSTEIN"

EXP[1]="./experiment_reference_methods.py"
EXP[2]="./experiment_scalability.py"
EXP[3]="./experiment_compression.py"
EXP[4]="./experiment_small_refs.py"

NAME[1]="REFERENCES"
NAME[2]="SACALABILITY"
NAME[3]="COMPRESSION"
NAME[4]="EXP1"

rm -rf ${DIR}
mkdir ${DIR}
for expid in "1" "2" "3" "4" ; do
  TOOL=${EXP[expid]}
  CURR_DIR=${DIR}/${NAME[expid]}
  rm -rf ${CURR_DIR};
  mkdir -p ${CURR_DIR}
  echo "*****************"
  echo "Testing ${TOOL}"
  echo "*****************"
  ${TOOL} ${FILE} ${CURR_DIR}
done
  
#rm -rf ${DIR};

echo ""
echo "***************************"
echo "Utest succesfully completed"
echo "***************************"
